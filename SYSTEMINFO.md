# Zoom

Vendor: Zoom
Homepage: https://Zoom.com/

Product: Zoom
Product Page: https://Zoom.com/

## Introduction
We classify Zoom into the Notifications domain as Zoom provides a messaging and communications platform including team chat and video conferencing. 

## Why Integrate
The Zoom adapter from Itential is used to integrate the Itential Automation Platform (IAP) with Zoom. With this adapter you have the ability to perform operations such as:

- Chat

## Additional Product Documentation
[API Documents for Zoom](https://developers.zoom.us/docs/api/)