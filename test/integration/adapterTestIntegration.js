/* @copyright Itential, LLC 2019 (pre-modifications) */

// Set globals
/* global describe it log pronghornProps */
/* eslint no-unused-vars: warn */
/* eslint no-underscore-dangle: warn  */
/* eslint import/no-dynamic-require:warn */

// include required items for testing & logging
const assert = require('assert');
const fs = require('fs');
const path = require('path');
const util = require('util');
const mocha = require('mocha');
const winston = require('winston');
const { expect } = require('chai');
const { use } = require('chai');
const td = require('testdouble');

const anything = td.matchers.anything();

// stub and attemptTimeout are used throughout the code so set them here
let logLevel = 'none';
const isRapidFail = false;
const isSaveMockData = false;

// read in the properties from the sampleProperties files
let adaptdir = __dirname;
if (adaptdir.endsWith('/test/integration')) {
  adaptdir = adaptdir.substring(0, adaptdir.length - 17);
} else if (adaptdir.endsWith('/test/unit')) {
  adaptdir = adaptdir.substring(0, adaptdir.length - 10);
}
const samProps = require(`${adaptdir}/sampleProperties.json`).properties;

// these variables can be changed to run in integrated mode so easier to set them here
// always check these in with bogus data!!!
samProps.stub = true;

// uncomment if connecting
// samProps.host = 'replace.hostorip.here';
// samProps.authentication.username = 'username';
// samProps.authentication.password = 'password';
// samProps.authentication.token = 'password';
// samProps.protocol = 'http';
// samProps.port = 80;
// samProps.ssl.enabled = false;
// samProps.ssl.accept_invalid_cert = false;

if (samProps.request.attempt_timeout < 30000) {
  samProps.request.attempt_timeout = 30000;
}
samProps.devicebroker.enabled = true;
const attemptTimeout = samProps.request.attempt_timeout;
const { stub } = samProps;

// these are the adapter properties. You generally should not need to alter
// any of these after they are initially set up
global.pronghornProps = {
  pathProps: {
    encrypted: false
  },
  adapterProps: {
    adapters: [{
      id: 'Test-zoom',
      type: 'Zoom',
      properties: samProps
    }]
  }
};

global.$HOME = `${__dirname}/../..`;

// set the log levels that Pronghorn uses, spam and trace are not defaulted in so without
// this you may error on log.trace calls.
const myCustomLevels = {
  levels: {
    spam: 6,
    trace: 5,
    debug: 4,
    info: 3,
    warn: 2,
    error: 1,
    none: 0
  }
};

// need to see if there is a log level passed in
process.argv.forEach((val) => {
  // is there a log level defined to be passed in?
  if (val.indexOf('--LOG') === 0) {
    // get the desired log level
    const inputVal = val.split('=')[1];

    // validate the log level is supported, if so set it
    if (Object.hasOwnProperty.call(myCustomLevels.levels, inputVal)) {
      logLevel = inputVal;
    }
  }
});

// need to set global logging
global.log = winston.createLogger({
  level: logLevel,
  levels: myCustomLevels.levels,
  transports: [
    new winston.transports.Console()
  ]
});

/**
 * Runs the common asserts for test
 */
function runCommonAsserts(data, error) {
  assert.equal(undefined, error);
  assert.notEqual(undefined, data);
  assert.notEqual(null, data);
  assert.notEqual(undefined, data.response);
  assert.notEqual(null, data.response);
}

/**
 * Runs the error asserts for the test
 */
function runErrorAsserts(data, error, code, origin, displayStr) {
  assert.equal(null, data);
  assert.notEqual(undefined, error);
  assert.notEqual(null, error);
  assert.notEqual(undefined, error.IAPerror);
  assert.notEqual(null, error.IAPerror);
  assert.notEqual(undefined, error.IAPerror.displayString);
  assert.notEqual(null, error.IAPerror.displayString);
  assert.equal(code, error.icode);
  assert.equal(origin, error.IAPerror.origin);
  assert.equal(displayStr, error.IAPerror.displayString);
}

/**
 * @function saveMockData
 * Attempts to take data from responses and place them in MockDataFiles to help create Mockdata.
 * Note, this was built based on entity file structure for Adapter-Engine 1.6.x
 * @param {string} entityName - Name of the entity saving mock data for
 * @param {string} actionName -  Name of the action saving mock data for
 * @param {string} descriptor -  Something to describe this test (used as a type)
 * @param {string or object} responseData - The data to put in the mock file.
 */
function saveMockData(entityName, actionName, descriptor, responseData) {
  // do not need to save mockdata if we are running in stub mode (already has mock data) or if told not to save
  if (stub || !isSaveMockData) {
    return false;
  }

  // must have a response in order to store the response
  if (responseData && responseData.response) {
    let data = responseData.response;

    // if there was a raw response that one is better as it is untranslated
    if (responseData.raw) {
      data = responseData.raw;

      try {
        const temp = JSON.parse(data);
        data = temp;
      } catch (pex) {
        // do not care if it did not parse as we will just use data
      }
    }

    try {
      const base = path.join(__dirname, `../../entities/${entityName}/`);
      const mockdatafolder = 'mockdatafiles';
      const filename = `mockdatafiles/${actionName}-${descriptor}.json`;

      if (!fs.existsSync(base + mockdatafolder)) {
        fs.mkdirSync(base + mockdatafolder);
      }

      // write the data we retrieved
      fs.writeFile(base + filename, JSON.stringify(data, null, 2), 'utf8', (errWritingMock) => {
        if (errWritingMock) throw errWritingMock;

        // update the action file to reflect the changes. Note: We're replacing the default object for now!
        fs.readFile(`${base}action.json`, (errRead, content) => {
          if (errRead) throw errRead;

          // parse the action file into JSON
          const parsedJson = JSON.parse(content);

          // The object update we'll write in.
          const responseObj = {
            type: descriptor,
            key: '',
            mockFile: filename
          };

          // get the object for method we're trying to change.
          const currentMethodAction = parsedJson.actions.find((obj) => obj.name === actionName);

          // if the method was not found - should never happen but...
          if (!currentMethodAction) {
            throw Error('Can\'t find an action for this method in the provided entity.');
          }

          // if there is a response object, we want to replace the Response object. Otherwise we'll create one.
          const actionResponseObj = currentMethodAction.responseObjects.find((obj) => obj.type === descriptor);

          // Add the action responseObj back into the array of response objects.
          if (!actionResponseObj) {
            // if there is a default response object, we want to get the key.
            const defaultResponseObj = currentMethodAction.responseObjects.find((obj) => obj.type === 'default');

            // save the default key into the new response object
            if (defaultResponseObj) {
              responseObj.key = defaultResponseObj.key;
            }

            // save the new response object
            currentMethodAction.responseObjects = [responseObj];
          } else {
            // update the location of the mock data file
            actionResponseObj.mockFile = responseObj.mockFile;
          }

          // Save results
          fs.writeFile(`${base}action.json`, JSON.stringify(parsedJson, null, 2), (err) => {
            if (err) throw err;
          });
        });
      });
    } catch (e) {
      log.debug(`Failed to save mock data for ${actionName}. ${e.message}`);
      return false;
    }
  }

  // no response to save
  log.debug(`No data passed to save into mockdata for ${actionName}`);
  return false;
}

// require the adapter that we are going to be using
const Zoom = require('../../adapter');

// begin the testing - these should be pretty well defined between the describe and the it!
describe('[integration] Zoom Adapter Test', () => {
  describe('Zoom Class Tests', () => {
    const a = new Zoom(
      pronghornProps.adapterProps.adapters[0].id,
      pronghornProps.adapterProps.adapters[0].properties
    );

    if (isRapidFail) {
      const state = {};
      state.passed = true;

      mocha.afterEach(function x() {
        state.passed = state.passed
        && (this.currentTest.state === 'passed');
      });
      mocha.beforeEach(function x() {
        if (!state.passed) {
          return this.currentTest.skip();
        }
        return true;
      });
    }

    describe('#class instance created', () => {
      it('should be a class with properties', (done) => {
        try {
          assert.notEqual(null, a);
          assert.notEqual(undefined, a);
          const checkId = global.pronghornProps.adapterProps.adapters[0].id;
          assert.equal(checkId, a.id);
          assert.notEqual(null, a.allProps);
          const check = global.pronghornProps.adapterProps.adapters[0].properties.healthcheck.type;
          assert.equal(check, a.healthcheckType);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#connect', () => {
      it('should get connected - no healthcheck', (done) => {
        try {
          a.healthcheckType = 'none';
          a.connect();

          try {
            assert.equal(true, a.alive);
            done();
          } catch (error) {
            log.error(`Test Failure: ${error}`);
            done(error);
          }
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      });
      it('should get connected - startup healthcheck', (done) => {
        try {
          a.healthcheckType = 'startup';
          a.connect();

          try {
            assert.equal(true, a.alive);
            done();
          } catch (error) {
            log.error(`Test Failure: ${error}`);
            done(error);
          }
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      });
    });

    describe('#healthCheck', () => {
      it('should be healthy', (done) => {
        try {
          a.healthCheck(null, (data) => {
            try {
              assert.equal(true, a.healthy);
              saveMockData('system', 'healthcheck', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    // broker tests
    describe('#getDevicesFiltered - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          const opts = {
            filter: {
              name: 'deviceName'
            }
          };
          a.getDevicesFiltered(opts, (data, error) => {
            try {
              if (stub) {
                if (samProps.devicebroker.getDevicesFiltered[0].handleFailure === 'ignore') {
                  assert.equal(null, error);
                  assert.notEqual(undefined, data);
                  assert.notEqual(null, data);
                  assert.equal(0, data.total);
                  assert.equal(0, data.list.length);
                } else {
                  const displayE = 'Error 400 received on request';
                  runErrorAsserts(data, error, 'AD.500', 'Test-zoom-connectorRest-handleEndResponse', displayE);
                }
              } else {
                runCommonAsserts(data, error);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#iapGetDeviceCount - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          const opts = {
            filter: {
              name: 'deviceName'
            }
          };
          a.iapGetDeviceCount((data, error) => {
            try {
              if (stub) {
                if (samProps.devicebroker.getDevicesFiltered[0].handleFailure === 'ignore') {
                  assert.equal(null, error);
                  assert.notEqual(undefined, data);
                  assert.notEqual(null, data);
                  assert.equal(0, data.count);
                } else {
                  const displayE = 'Error 400 received on request';
                  runErrorAsserts(data, error, 'AD.500', 'Test-zoom-connectorRest-handleEndResponse', displayE);
                }
              } else {
                runCommonAsserts(data, error);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    // exposed cache tests
    describe('#iapPopulateEntityCache - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.iapPopulateEntityCache('Device', (data, error) => {
            try {
              if (stub) {
                assert.equal(null, data);
                assert.notEqual(undefined, error);
                assert.notEqual(null, error);
                done();
              } else {
                assert.equal(undefined, error);
                assert.equal('success', data[0]);
                done();
              }
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#iapRetrieveEntitiesCache - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.iapRetrieveEntitiesCache('Device', {}, (data, error) => {
            try {
              if (stub) {
                assert.equal(null, data);
                assert.notEqual(null, error);
                assert.notEqual(undefined, error);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(null, data);
                assert.notEqual(undefined, data);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });
    /*
    -----------------------------------------------------------------------
    -----------------------------------------------------------------------
    *** All code above this comment will be replaced during a migration ***
    ******************* DO NOT REMOVE THIS COMMENT BLOCK ******************
    -----------------------------------------------------------------------
    -----------------------------------------------------------------------
    */

    let accountsAccountId = 'fakedata';
    const accountsAccountCreateBodyParam = {
      first_name: 'string',
      last_name: 'string',
      email: 'string',
      password: 'string'
    };
    describe('#accountCreate - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.accountCreate(accountsAccountCreateBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.owner_id);
                assert.equal('string', data.response.owner_email);
                assert.equal('string', data.response.created_at);
              } else {
                runCommonAsserts(data, error);
              }
              accountsAccountId = data.response.id;
              saveMockData('Accounts', 'accountCreate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAccounts - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAccounts(null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(8, data.response.page_count);
                assert.equal(1, data.response.pageNumber);
                assert.equal(30, data.response.pageSize);
                assert.equal(4, data.response.total_records);
                assert.equal(true, Array.isArray(data.response.accounts));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Accounts', 'getAccounts', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#account - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.account(accountsAccountId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.owner_id);
                assert.equal('string', data.response.owner_email);
                assert.equal('string', data.response.created_at);
                assert.equal('object', typeof data.response.options);
                assert.equal('string', data.response.vanity_url);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Accounts', 'account', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#accountManagedDomain - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.accountManagedDomain(accountsAccountId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(2, data.response.total_records);
                assert.equal(true, Array.isArray(data.response.domains));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Accounts', 'accountManagedDomain', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const accountsAccountOptionsUpdateBodyParam = {
      share_rc: false,
      room_connectors: 'string',
      share_mc: false,
      meeting_connectors: 'string',
      pay_mode: 'master'
    };
    describe('#accountOptionsUpdate - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.accountOptionsUpdate(accountsAccountId, accountsAccountOptionsUpdateBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-zoom-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Accounts', 'accountOptionsUpdate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const accountsAccountSettingsUpdateBodyParam = {
      schedule_meting: {
        host_video: false,
        participant_video: false,
        audio_type: 'both',
        join_before_host: false,
        enforce_login: true,
        enforce_login_with_domains: false,
        enforce_login_domains: 'string',
        not_store_meeting_topic: true,
        force_pmi_jbh_password: true
      },
      in_meeting: {
        e2e_encryption: true,
        chat: true,
        private_chat: true,
        auto_saving_chat: true,
        file_transfer: false,
        feedback: true,
        post_meeting_feedback: true,
        co_host: false,
        polling: true,
        attendee_on_hold: false,
        show_meeting_control_toolbar: false,
        allow_show_zoom_windows: false,
        annotation: true,
        whiteboard: false,
        webinar_question_answer: false,
        anonymous_question_answer: false,
        breakout_room: false,
        closed_caption: false,
        far_end_camera_control: false,
        group_hd: false,
        virtual_background: true,
        watermark: true,
        alert_guest_join: false,
        auto_answer: true,
        p2p_connetion: true,
        p2p_ports: true,
        ports_range: 'string',
        sending_default_email_invites: true,
        use_html_format_email: false,
        dscp_marking: false,
        dscp_audio: 8,
        dscp_video: 1,
        stereo_audio: true,
        original_audio: true,
        screen_sharing: true,
        remote_control: true,
        attention_tracking: true,
        allow_live_streaming: false,
        workplace_by_facebook: false,
        custom_live_streaming: false,
        custom_service_instructions: 'string'
      },
      email_notification: {
        cloud_recording_avaliable_reminder: true,
        jbh_reminder: true,
        cancel_meeting_reminder: true,
        low_host_count_reminder: false,
        alternative_host_reminder: false
      },
      zoom_rooms: {
        upcoming_meeting_alert: false,
        start_airplay_manually: false,
        weekly_system_restart: true,
        list_meetings_with_calendar: false,
        zr_post_meeting_feedback: false,
        ultrasonic: false,
        force_private_meeting: false,
        hide_host_information: true,
        cmr_for_instant_meeting: true,
        auto_start_stop_scheduled_meetings: true
      },
      security: {
        admin_change_name_pic: false,
        import_photos_from_devices: false,
        hide_billing_info: true
      },
      recording: {
        local_recording: true,
        cloud_recording: false,
        record_speaker_view: false,
        record_gallery_view: true,
        record_audio_file: false,
        save_chat_text: true,
        show_timestamp: true,
        recording_audio_transcript: false,
        auto_recording: 'cloud',
        cloud_recording_download: true,
        cloud_recording_download_host: false,
        account_user_access_recording: false,
        auto_delete_cmr: true,
        auto_delete_cmr_days: 5
      },
      telephony: {
        third_party_audio: false,
        audio_conference_info: 'string'
      },
      integration: {
        google_calendar: true,
        google_drive: true,
        dropbox: false,
        box: false,
        microsoft_one_drive: false,
        kubi: false
      },
      feature: {
        meeting_capacity: 100
      }
    };
    describe('#accountSettingsUpdate - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.accountSettingsUpdate(accountsAccountId, accountsAccountSettingsUpdateBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-zoom-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Accounts', 'accountSettingsUpdate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#accountSettings - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.accountSettings(accountsAccountId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.schedule_meting);
                assert.equal('object', typeof data.response.in_meeting);
                assert.equal('object', typeof data.response.email_notification);
                assert.equal('object', typeof data.response.zoom_rooms);
                assert.equal('object', typeof data.response.security);
                assert.equal('object', typeof data.response.recording);
                assert.equal('object', typeof data.response.telephony);
                assert.equal('object', typeof data.response.integration);
                assert.equal('object', typeof data.response.feature);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Accounts', 'accountSettings', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const billingAccountId = 'fakedata';
    const billingAccountPlanCreateBodyParam = {
      contact: {
        first_name: 'string',
        last_name: 'string',
        email: 'string',
        phone_number: 'string',
        address: 'string',
        apt: 'string',
        city: 'string',
        state: 'string',
        zip: 'string',
        country: 'string'
      },
      plan_base: {
        type: 'string',
        hosts: 8
      },
      plan_zoom_rooms: {
        type: 'string',
        hosts: 3
      },
      plan_room_connector: {
        type: 'string',
        hosts: 5
      },
      plan_large_meeting: [
        {
          type: 'string',
          hosts: 3
        }
      ],
      plan_webinar: [
        {
          type: 'string',
          hosts: 9
        }
      ],
      plan_recording: 'string',
      plan_audio: {
        type: 'string',
        tollfree_countries: 'string',
        premium_countries: 'string',
        callout_countries: 'string',
        ddi_numbers: 10
      }
    };
    describe('#accountPlanCreate - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.accountPlanCreate(billingAccountId, billingAccountPlanCreateBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.plan_base);
                assert.equal('object', typeof data.response.plan_zoom_rooms);
                assert.equal('object', typeof data.response.plan_room_connector);
                assert.equal(true, Array.isArray(data.response.plan_large_meeting));
                assert.equal(true, Array.isArray(data.response.plan_webinar));
                assert.equal('string', data.response.plan_recording);
                assert.equal('object', typeof data.response.plan_audio);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Billing', 'accountPlanCreate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const billingAccountPlanAddonCreateBodyParam = {
      type: 'string',
      hosts: 2
    };
    describe('#accountPlanAddonCreate - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.accountPlanAddonCreate(billingAccountId, billingAccountPlanAddonCreateBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-zoom-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Billing', 'accountPlanAddonCreate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const billingAccountBillingUpdateBodyParam = {
      first_name: 'string',
      last_name: 'string',
      email: 'string',
      phone_number: 'string',
      address: 'string',
      apt: 'string',
      city: 'string',
      state: 'string',
      zip: 'string',
      country: 'string'
    };
    describe('#accountBillingUpdate - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.accountBillingUpdate(billingAccountId, billingAccountBillingUpdateBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-zoom-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Billing', 'accountBillingUpdate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#accountBilling - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.accountBilling(billingAccountId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.first_name);
                assert.equal('string', data.response.last_name);
                assert.equal('string', data.response.email);
                assert.equal('string', data.response.phone_number);
                assert.equal('string', data.response.address);
                assert.equal('string', data.response.apt);
                assert.equal('string', data.response.city);
                assert.equal('string', data.response.state);
                assert.equal('string', data.response.zip);
                assert.equal('string', data.response.country);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Billing', 'accountBilling', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#accountPlans - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.accountPlans(billingAccountId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.plan_base);
                assert.equal('object', typeof data.response.plan_zoom_rooms);
                assert.equal('object', typeof data.response.plan_room_connector);
                assert.equal(true, Array.isArray(data.response.plan_large_meeting));
                assert.equal(true, Array.isArray(data.response.plan_webinar));
                assert.equal('string', data.response.plan_recording);
                assert.equal('object', typeof data.response.plan_audio);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Billing', 'accountPlans', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const billingAccountPlanAddonUpdateBodyParam = {
      type: 'string',
      hosts: 5
    };
    describe('#accountPlanAddonUpdate - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.accountPlanAddonUpdate(billingAccountId, billingAccountPlanAddonUpdateBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-zoom-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Billing', 'accountPlanAddonUpdate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const billingAccountPlanBaseUpdateBodyParam = {
      type: 'string',
      hosts: 9
    };
    describe('#accountPlanBaseUpdate - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.accountPlanBaseUpdate(billingAccountId, billingAccountPlanBaseUpdateBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-zoom-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Billing', 'accountPlanBaseUpdate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    let groupsGroupId = 'fakedata';
    let groupsMemberId = 'fakedata';
    const groupsGroupCreateBodyParam = {
      name: 'string'
    };
    describe('#groupCreate - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.groupCreate(groupsGroupCreateBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.name);
                assert.equal(8, data.response.total_members);
              } else {
                runCommonAsserts(data, error);
              }
              groupsGroupId = data.response.id;
              groupsMemberId = data.response.id;
              saveMockData('Groups', 'groupCreate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const groupsGroupMembersCreateBodyParam = {
      members: [
        {
          id: 'string',
          email: 'string'
        }
      ]
    };
    describe('#groupMembersCreate - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.groupMembersCreate(groupsGroupId, groupsGroupMembersCreateBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.ids);
                assert.equal('string', data.response.added_at);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Groups', 'groupMembersCreate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getGroups - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getGroups((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(2, data.response.total_records);
                assert.equal(true, Array.isArray(data.response.groups));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Groups', 'getGroups', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const groupsGroupUpdateBodyParam = {
      name: 'string'
    };
    describe('#groupUpdate - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.groupUpdate(groupsGroupId, groupsGroupUpdateBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-zoom-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Groups', 'groupUpdate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#group - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.group(groupsGroupId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.name);
                assert.equal(8, data.response.total_members);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Groups', 'group', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#groupMembers - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.groupMembers(groupsGroupId, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(4, data.response.page_count);
                assert.equal(1, data.response.pageNumber);
                assert.equal(30, data.response.pageSize);
                assert.equal(8, data.response.total_records);
                assert.equal(true, Array.isArray(data.response.members));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Groups', 'groupMembers', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    let devicesDeviceId = 'fakedata';
    const devicesDeviceCreateBodyParam = {
      name: 'string',
      protocol: 'SIP',
      ip: 'string',
      encryption: 'auto'
    };
    describe('#deviceCreate - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deviceCreate(devicesDeviceCreateBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.name);
                assert.equal('H.323', data.response.protocol);
                assert.equal('string', data.response.ip);
                assert.equal('yes', data.response.encryption);
              } else {
                runCommonAsserts(data, error);
              }
              devicesDeviceId = data.response.id;
              saveMockData('Devices', 'deviceCreate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deviceList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deviceList((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(1, data.response.page_count);
                assert.equal(1, data.response.page_number);
                assert.equal(30, data.response.page_size);
                assert.equal(8, data.response.total_records);
                assert.equal(true, Array.isArray(data.response.devices));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Devices', 'deviceList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const devicesDeviceUpdateBodyParam = {
      name: 'string',
      protocol: 'SIP',
      ip: 'string',
      encryption: 'yes'
    };
    describe('#deviceUpdate - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deviceUpdate(devicesDeviceId, devicesDeviceUpdateBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-zoom-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Devices', 'deviceUpdate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    let trackingFieldFieldId = 'fakedata';
    const trackingFieldTrackingfieldCreateBodyParam = {
      field: 'string',
      required: true,
      visible: true,
      recommended_values: [
        'string'
      ]
    };
    describe('#trackingfieldCreate - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.trackingfieldCreate(trackingFieldTrackingfieldCreateBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.field);
                assert.equal(false, data.response.required);
                assert.equal(false, data.response.visible);
                assert.equal(true, Array.isArray(data.response.recommended_values));
              } else {
                runCommonAsserts(data, error);
              }
              trackingFieldFieldId = data.response.id;
              saveMockData('TrackingField', 'trackingfieldCreate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#trackingfieldList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.trackingfieldList((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(8, data.response.total_records);
                assert.equal(true, Array.isArray(data.response.tracking_fields));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('TrackingField', 'trackingfieldList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const trackingFieldTrackingfieldUpdateBodyParam = {
      field: 'string',
      required: true,
      visible: false,
      recommended_values: [
        'string'
      ]
    };
    describe('#trackingfieldUpdate - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.trackingfieldUpdate(trackingFieldFieldId, trackingFieldTrackingfieldUpdateBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-zoom-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('TrackingField', 'trackingfieldUpdate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#trackingfieldGet - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.trackingfieldGet(trackingFieldFieldId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.field);
                assert.equal(true, data.response.required);
                assert.equal(true, data.response.visible);
                assert.equal(true, Array.isArray(data.response.recommended_values));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('TrackingField', 'trackingfieldGet', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    let iMGroupsGroupId = 'fakedata';
    let iMGroupsMemberId = 'fakedata';
    const iMGroupsImGroupCreateBodyParam = {
      name: 'string',
      type: 'normal',
      search_by_domain: true,
      search_by_account: true,
      search_by_ma_account: true
    };
    describe('#imGroupCreate - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.imGroupCreate(iMGroupsImGroupCreateBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.name);
                assert.equal(9, data.response.total_members);
                assert.equal(true, data.response.search_by_domain);
                assert.equal(true, data.response.search_by_account);
                assert.equal(true, data.response.search_by_ma_account);
              } else {
                runCommonAsserts(data, error);
              }
              iMGroupsGroupId = data.response.id;
              iMGroupsMemberId = data.response.id;
              saveMockData('IMGroups', 'imGroupCreate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const iMGroupsImGroupMembersCreateBodyParam = {
      members: [
        {
          id: 'string',
          email: 'string'
        }
      ]
    };
    describe('#imGroupMembersCreate - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.imGroupMembersCreate(iMGroupsGroupId, iMGroupsImGroupMembersCreateBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.ids);
                assert.equal('string', data.response.added_at);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('IMGroups', 'imGroupMembersCreate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#imGroups - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.imGroups((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(10, data.response.page_count);
                assert.equal(1, data.response.pageNumber);
                assert.equal(30, data.response.pageSize);
                assert.equal(9, data.response.total_records);
                assert.equal(true, Array.isArray(data.response.groups));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('IMGroups', 'imGroups', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const iMGroupsImGroupUpdateBodyParam = {
      name: 'string',
      type: 'restricted',
      search_by_domain: true,
      search_by_account: false,
      search_by_ma_account: false
    };
    describe('#imGroupUpdate - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.imGroupUpdate(iMGroupsGroupId, iMGroupsImGroupUpdateBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-zoom-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('IMGroups', 'imGroupUpdate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#imGroup - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.imGroup(iMGroupsGroupId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.name);
                assert.equal(3, data.response.total_members);
                assert.equal('normal', data.response.type);
                assert.equal(true, data.response.search_by_domain);
                assert.equal(false, data.response.search_by_account);
                assert.equal(false, data.response.search_by_ma_account);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('IMGroups', 'imGroup', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#imGroupMembers - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.imGroupMembers(iMGroupsGroupId, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(7, data.response.page_count);
                assert.equal(1, data.response.pageNumber);
                assert.equal(30, data.response.pageSize);
                assert.equal(9, data.response.total_records);
                assert.equal(true, Array.isArray(data.response.members));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('IMGroups', 'imGroupMembers', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#imChatSessions - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.imChatSessions('fakedata', 'fakedata', null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.from);
                assert.equal('string', data.response.to);
                assert.equal(30, data.response.pageSize);
                assert.equal('string', data.response.nextPageToken);
                assert.equal(true, Array.isArray(data.response.sessions));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('IMChat', 'imChatSessions', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const iMChatSessionId = 'fakedata';
    describe('#imChatMessages - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.imChatMessages(iMChatSessionId, 'fakedata', 'fakedata', null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.session_id);
                assert.equal('string', data.response.from);
                assert.equal('string', data.response.to);
                assert.equal(30, data.response.pageSize);
                assert.equal('string', data.response.nextPageToken);
                assert.equal(true, Array.isArray(data.response.messages));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('IMChat', 'imChatMessages', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    let meetingsMeetingId = 555;
    let meetingsPollId = 'fakedata';
    const meetingsMeetingPollCreateBodyParam = {
      title: 'string',
      questions: [
        {
          name: 'string',
          type: 'multiple',
          answers: [
            'string'
          ]
        }
      ]
    };
    describe('#meetingPollCreate - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.meetingPollCreate(meetingsMeetingId, meetingsMeetingPollCreateBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.id);
                assert.equal('ended', data.response.status);
                assert.equal('string', data.response.title);
                assert.equal(true, Array.isArray(data.response.questions));
              } else {
                runCommonAsserts(data, error);
              }
              meetingsMeetingId = data.response.id;
              meetingsPollId = data.response.id;
              saveMockData('Meetings', 'meetingPollCreate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const meetingsMeetingRegistrantCreateBodyParam = {
      email: 'string',
      first_name: 'string',
      last_name: 'string',
      address: 'string',
      city: 'string',
      country: 'string',
      zip: 'string',
      state: 'string',
      phone: 'string',
      industry: 'string',
      org: 'string',
      job_title: 'string',
      purchasing_time_frame: 'No timeframe',
      role_in_purchase_process: 'Influencer',
      no_of_employees: '1-20',
      comments: 'string',
      custom_questions: [
        {
          title: 'string',
          value: 'string'
        }
      ]
    };
    describe('#meetingRegistrantCreate - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.meetingRegistrantCreate(meetingsMeetingId, null, meetingsMeetingRegistrantCreateBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.registrant_id);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.topic);
                assert.equal('string', data.response.start_time);
                assert.equal('string', data.response.join_url);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Meetings', 'meetingRegistrantCreate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    let meetingsUserId = 'fakedata';
    const meetingsMeetingCreateBodyParam = {
      schedule_for: 'string',
      topic: 'string',
      type: 2,
      start_time: 'string',
      duration: 7,
      timezone: 'string',
      password: 'string',
      agenda: 'string',
      tracking_fields: [
        {
          field: 'string',
          value: 'string'
        }
      ],
      recurrence: {
        type: 1,
        repeat_interval: 4,
        weekly_days: 5,
        monthly_day: 6,
        monthly_week: 4,
        monthly_week_day: 5,
        end_times: 1,
        end_date_time: 'string'
      },
      settings: {
        host_video: true,
        participant_video: true,
        cn_meeting: true,
        in_meeting: true,
        join_before_host: true,
        mute_upon_entry: true,
        watermark: false,
        use_pmi: true,
        approval_type: 2,
        registration_type: 1,
        audio: 'both',
        auto_recording: 'none',
        enforce_login: true,
        enforce_login_domains: 'string',
        alternative_hosts: 'string',
        close_registration: false,
        waiting_room: false
      }
    };
    describe('#meetingCreate - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.meetingCreate(meetingsUserId, meetingsMeetingCreateBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.uuid);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.host_id);
                assert.equal('string', data.response.topic);
                assert.equal(2, data.response.type);
                assert.equal('string', data.response.start_time);
                assert.equal(8, data.response.duration);
                assert.equal('string', data.response.timezone);
                assert.equal('string', data.response.created_at);
                assert.equal('string', data.response.agenda);
                assert.equal('string', data.response.start_url);
                assert.equal('string', data.response.join_url);
                assert.equal('string', data.response.password);
                assert.equal('string', data.response.h323_password);
                assert.equal(true, Array.isArray(data.response.tracking_fields));
                assert.equal(true, Array.isArray(data.response.occurrences));
                assert.equal('object', typeof data.response.settings);
              } else {
                runCommonAsserts(data, error);
              }
              meetingsUserId = data.response.id;
              saveMockData('Meetings', 'meetingCreate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const meetingsMeetingUpdateBodyParam = {
      schedule_for: 'string',
      topic: 'string',
      type: 2,
      start_time: 'string',
      duration: 8,
      timezone: 'string',
      password: 'string',
      agenda: 'string',
      tracking_fields: [
        {
          field: 'string',
          value: 'string'
        }
      ],
      recurrence: {
        type: 2,
        repeat_interval: 4,
        weekly_days: 3,
        monthly_day: 4,
        monthly_week: 1,
        monthly_week_day: 2,
        end_times: 1,
        end_date_time: 'string'
      },
      settings: {
        host_video: true,
        participant_video: false,
        cn_meeting: false,
        in_meeting: true,
        join_before_host: false,
        mute_upon_entry: false,
        watermark: false,
        use_pmi: false,
        approval_type: 2,
        registration_type: 1,
        audio: 'both',
        auto_recording: 'none',
        enforce_login: true,
        enforce_login_domains: 'string',
        alternative_hosts: 'string',
        close_registration: false,
        waiting_room: false,
        registrants_confirmation_email: true
      }
    };
    describe('#meetingUpdate - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.meetingUpdate(meetingsMeetingId, meetingsMeetingUpdateBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-zoom-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Meetings', 'meetingUpdate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#meeting - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.meeting(meetingsMeetingId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.uuid);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.host_id);
                assert.equal('string', data.response.topic);
                assert.equal(2, data.response.type);
                assert.equal('string', data.response.start_time);
                assert.equal(3, data.response.duration);
                assert.equal('string', data.response.timezone);
                assert.equal('string', data.response.created_at);
                assert.equal('string', data.response.agenda);
                assert.equal('string', data.response.start_url);
                assert.equal('string', data.response.join_url);
                assert.equal('string', data.response.password);
                assert.equal('string', data.response.h323_password);
                assert.equal(true, Array.isArray(data.response.tracking_fields));
                assert.equal(true, Array.isArray(data.response.occurrences));
                assert.equal('object', typeof data.response.settings);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Meetings', 'meeting', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#meetingInvitation - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.meetingInvitation(meetingsMeetingId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.invitation);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Meetings', 'meetingInvitation', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const meetingsMeetingLiveStreamUpdateBodyParam = {
      stream_url: 'string',
      stream_key: 'string'
    };
    describe('#meetingLiveStreamUpdate - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.meetingLiveStreamUpdate(meetingsMeetingId, meetingsMeetingLiveStreamUpdateBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-zoom-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Meetings', 'meetingLiveStreamUpdate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const meetingsMeetingLiveStreamStatusUpdateBodyParam = {
      action: 'stop',
      settings: {
        active_speaker_name: true,
        display_name: 'string'
      }
    };
    describe('#meetingLiveStreamStatusUpdate - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.meetingLiveStreamStatusUpdate(meetingsMeetingId, meetingsMeetingLiveStreamStatusUpdateBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-zoom-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Meetings', 'meetingLiveStreamStatusUpdate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#meetingPolls - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.meetingPolls(meetingsMeetingId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(7, data.response.total_records);
                assert.equal(true, Array.isArray(data.response.polls));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Meetings', 'meetingPolls', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const meetingsMeetingPollUpdateBodyParam = {
      title: 'string',
      questions: [
        {
          name: 'string',
          type: 'multiple',
          answers: [
            'string'
          ]
        }
      ]
    };
    describe('#meetingPollUpdate - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.meetingPollUpdate(meetingsMeetingId, meetingsPollId, meetingsMeetingPollUpdateBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-zoom-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Meetings', 'meetingPollUpdate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#meetingPollGet - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.meetingPollGet(meetingsMeetingId, meetingsPollId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.id);
                assert.equal('ended', data.response.status);
                assert.equal('string', data.response.title);
                assert.equal(true, Array.isArray(data.response.questions));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Meetings', 'meetingPollGet', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#meetingRegistrants - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.meetingRegistrants(meetingsMeetingId, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(10, data.response.page_count);
                assert.equal(1, data.response.pageNumber);
                assert.equal(30, data.response.pageSize);
                assert.equal(10, data.response.total_records);
                assert.equal(true, Array.isArray(data.response.registrants));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Meetings', 'meetingRegistrants', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const meetingsMeetingRegistrantStatusBodyParam = {
      action: 'approve'
    };
    describe('#meetingRegistrantStatus - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.meetingRegistrantStatus(meetingsMeetingId, null, meetingsMeetingRegistrantStatusBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-zoom-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Meetings', 'meetingRegistrantStatus', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const meetingsMeetingStatusBodyParam = {
      action: 'end'
    };
    describe('#meetingStatus - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.meetingStatus(meetingsMeetingId, meetingsMeetingStatusBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-zoom-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Meetings', 'meetingStatus', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#pastMeetings - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.pastMeetings(meetingsMeetingId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.meetings));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Meetings', 'pastMeetings', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const meetingsMeetingUUID = 'fakedata';
    describe('#pastMeetingDetails - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.pastMeetingDetails(meetingsMeetingUUID, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.uuid);
                assert.equal(7, data.response.id);
                assert.equal(7, data.response.host_id);
                assert.equal(9, data.response.type);
                assert.equal('string', data.response.topic);
                assert.equal('string', data.response.user_name);
                assert.equal('string', data.response.user_email);
                assert.equal('string', data.response.start_time);
                assert.equal('string', data.response.end_time);
                assert.equal(6, data.response.duration);
                assert.equal(4, data.response.total_minutes);
                assert.equal(1, data.response.participants_count);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Meetings', 'pastMeetingDetails', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#pastMeetingParticipants - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.pastMeetingParticipants(meetingsMeetingUUID, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(4, data.response.page_count);
                assert.equal(30, data.response.pageSize);
                assert.equal(3, data.response.total_records);
                assert.equal('string', data.response.nextPageToken);
                assert.equal(true, Array.isArray(data.response.participants));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Meetings', 'pastMeetingParticipants', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#meetings - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.meetings(meetingsUserId, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(6, data.response.page_count);
                assert.equal(1, data.response.pageNumber);
                assert.equal(30, data.response.pageSize);
                assert.equal(2, data.response.total_records);
                assert.equal(true, Array.isArray(data.response.meetings));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Meetings', 'meetings', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const cloudRecordingMeetingId = 'fakedata';
    describe('#recordingGet - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.recordingGet(cloudRecordingMeetingId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.uuid);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.account_id);
                assert.equal('string', data.response.host_id);
                assert.equal('string', data.response.topic);
                assert.equal('string', data.response.start_time);
                assert.equal(2, data.response.duration);
                assert.equal('string', data.response.total_size);
                assert.equal('string', data.response.recording_count);
                assert.equal(true, Array.isArray(data.response.recording_files));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CloudRecording', 'recordingGet', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const cloudRecordingRecordingSettingsUpdateBodyParam = {
      share_recording: 'none',
      viewer_download: false,
      password: 'string',
      on_demand: false,
      approval_type: 2,
      send_email_to_host: true,
      show_social_share_buttons: true
    };
    describe('#recordingSettingsUpdate - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.recordingSettingsUpdate(cloudRecordingMeetingId, cloudRecordingRecordingSettingsUpdateBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-zoom-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CloudRecording', 'recordingSettingsUpdate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#recordingSettingUpdate - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.recordingSettingUpdate(cloudRecordingMeetingId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('none', data.response.share_recording);
                assert.equal(false, data.response.viewer_download);
                assert.equal('string', data.response.password);
                assert.equal(false, data.response.on_demand);
                assert.equal(0, data.response.approval_type);
                assert.equal(false, data.response.send_email_to_host);
                assert.equal(true, data.response.show_social_share_buttons);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CloudRecording', 'recordingSettingUpdate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const cloudRecordingRecordingStatusUpdateBodyParam = {
      action: 'recover'
    };
    describe('#recordingStatusUpdate - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.recordingStatusUpdate(cloudRecordingMeetingId, cloudRecordingRecordingStatusUpdateBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-zoom-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CloudRecording', 'recordingStatusUpdate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const cloudRecordingRecordingId = 'fakedata';
    const cloudRecordingRecordingStatusUpdateOneBodyParam = {
      action: 'recover'
    };
    describe('#recordingStatusUpdateOne - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.recordingStatusUpdateOne(cloudRecordingMeetingId, cloudRecordingRecordingId, cloudRecordingRecordingStatusUpdateOneBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-zoom-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CloudRecording', 'recordingStatusUpdateOne', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const cloudRecordingUserId = 'fakedata';
    describe('#recordingsList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.recordingsList(cloudRecordingUserId, 'fakedata', 'fakedata', null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.from);
                assert.equal('string', data.response.to);
                assert.equal(2, data.response.page_count);
                assert.equal(30, data.response.pageSize);
                assert.equal(2, data.response.total_records);
                assert.equal('string', data.response.nextPageToken);
                assert.equal(true, Array.isArray(data.response.meetings));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CloudRecording', 'recordingsList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#dashboardCRC - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.dashboardCRC('fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.from);
                assert.equal('string', data.response.to);
                assert.equal(true, Array.isArray(data.response.crc_ports_usage));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dashboards', 'dashboardCRC', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#dashboardIM - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.dashboardIM('fakedata', 'fakedata', null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.from);
                assert.equal('string', data.response.to);
                assert.equal(4, data.response.page_count);
                assert.equal(30, data.response.pageSize);
                assert.equal(6, data.response.total_records);
                assert.equal('string', data.response.nextPageToken);
                assert.equal(true, Array.isArray(data.response.users));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dashboards', 'dashboardIM', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#dashboardMeetings - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.dashboardMeetings(null, 'fakedata', 'fakedata', null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.from);
                assert.equal('string', data.response.to);
                assert.equal(10, data.response.page_count);
                assert.equal(30, data.response.pageSize);
                assert.equal(3, data.response.total_records);
                assert.equal('string', data.response.nextPageToken);
                assert.equal(true, Array.isArray(data.response.meetings));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dashboards', 'dashboardMeetings', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const dashboardsMeetingId = 'fakedata';
    describe('#dashboardMeetingDetail - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.dashboardMeetingDetail(dashboardsMeetingId, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.uuid);
                assert.equal(10, data.response.id);
                assert.equal('string', data.response.topic);
                assert.equal('string', data.response.host);
                assert.equal('string', data.response.email);
                assert.equal('string', data.response.user_type);
                assert.equal('string', data.response.start_time);
                assert.equal('string', data.response.end_time);
                assert.equal('string', data.response.duration);
                assert.equal(10, data.response.participants);
                assert.equal(false, data.response.has_pstn);
                assert.equal(true, data.response.has_voip);
                assert.equal(false, data.response.has_3rd_party_audio);
                assert.equal(false, data.response.has_video);
                assert.equal(false, data.response.has_screen_share);
                assert.equal(true, data.response.has_recording);
                assert.equal(false, data.response.has_sip);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dashboards', 'dashboardMeetingDetail', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#dashboardMeetingParticipants - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.dashboardMeetingParticipants(dashboardsMeetingId, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(1, data.response.page_count);
                assert.equal(30, data.response.pageSize);
                assert.equal(1, data.response.total_records);
                assert.equal('string', data.response.nextPageToken);
                assert.equal(true, Array.isArray(data.response.participants));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dashboards', 'dashboardMeetingParticipants', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#dashboardMeetingParticipantsQOS - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.dashboardMeetingParticipantsQOS(dashboardsMeetingId, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(2, data.response.page_count);
                assert.equal(1, data.response.pageSize);
                assert.equal(8, data.response.total_records);
                assert.equal('string', data.response.nextPageToken);
                assert.equal(true, Array.isArray(data.response.participants));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dashboards', 'dashboardMeetingParticipantsQOS', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#dashboardMeetingParticipantShare - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.dashboardMeetingParticipantShare(dashboardsMeetingId, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(2, data.response.page_count);
                assert.equal(30, data.response.pageSize);
                assert.equal(6, data.response.total_records);
                assert.equal('string', data.response.nextPageToken);
                assert.equal(true, Array.isArray(data.response.participants));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dashboards', 'dashboardMeetingParticipantShare', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const dashboardsParticipantId = 'fakedata';
    describe('#dashboardMeetingParticipantQOS - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.dashboardMeetingParticipantQOS(dashboardsMeetingId, dashboardsParticipantId, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.user_id);
                assert.equal('string', data.response.user_name);
                assert.equal('string', data.response.device);
                assert.equal('string', data.response.ip_address);
                assert.equal('string', data.response.location);
                assert.equal('string', data.response.join_time);
                assert.equal('string', data.response.leave_time);
                assert.equal('string', data.response.pc_name);
                assert.equal('string', data.response.domain);
                assert.equal('string', data.response.mac_addr);
                assert.equal('string', data.response.harddisk_id);
                assert.equal('string', data.response.version);
                assert.equal('object', typeof data.response.user_qos);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dashboards', 'dashboardMeetingParticipantQOS', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#dashboardWebinars - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.dashboardWebinars(null, 'fakedata', 'fakedata', null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.from);
                assert.equal('string', data.response.to);
                assert.equal(4, data.response.page_count);
                assert.equal(30, data.response.pageSize);
                assert.equal(9, data.response.total_records);
                assert.equal('string', data.response.nextPageToken);
                assert.equal(true, Array.isArray(data.response.webinars));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dashboards', 'dashboardWebinars', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const dashboardsWebinarId = 'fakedata';
    describe('#dashboardWebinarDetail - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.dashboardWebinarDetail(dashboardsWebinarId, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.uuid);
                assert.equal(7, data.response.id);
                assert.equal('string', data.response.topic);
                assert.equal('string', data.response.host);
                assert.equal('string', data.response.email);
                assert.equal('string', data.response.user_type);
                assert.equal('string', data.response.start_time);
                assert.equal('string', data.response.end_time);
                assert.equal('string', data.response.duration);
                assert.equal(8, data.response.participants);
                assert.equal(true, data.response.has_pstn);
                assert.equal(false, data.response.has_voip);
                assert.equal(false, data.response.has_3rd_party_audio);
                assert.equal(true, data.response.has_video);
                assert.equal(true, data.response.has_screen_share);
                assert.equal(false, data.response.has_recording);
                assert.equal(false, data.response.has_sip);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dashboards', 'dashboardWebinarDetail', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#dashboardWebinarParticipants - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.dashboardWebinarParticipants(dashboardsWebinarId, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(6, data.response.page_count);
                assert.equal(30, data.response.pageSize);
                assert.equal(5, data.response.total_records);
                assert.equal('string', data.response.nextPageToken);
                assert.equal(true, Array.isArray(data.response.participants));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dashboards', 'dashboardWebinarParticipants', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#dashboardWebinarParticipantsQOS - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.dashboardWebinarParticipantsQOS(dashboardsWebinarId, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(7, data.response.page_count);
                assert.equal(1, data.response.pageSize);
                assert.equal(2, data.response.total_records);
                assert.equal('string', data.response.nextPageToken);
                assert.equal(true, Array.isArray(data.response.participants));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dashboards', 'dashboardWebinarParticipantsQOS', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#dashboardWebinarParticipantShare - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.dashboardWebinarParticipantShare(dashboardsWebinarId, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(4, data.response.page_count);
                assert.equal(30, data.response.pageSize);
                assert.equal(5, data.response.total_records);
                assert.equal('string', data.response.nextPageToken);
                assert.equal(true, Array.isArray(data.response.participants));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dashboards', 'dashboardWebinarParticipantShare', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#dashboardWebinarParticipantQOS - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.dashboardWebinarParticipantQOS(dashboardsWebinarId, dashboardsParticipantId, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.user_id);
                assert.equal('string', data.response.user_name);
                assert.equal('string', data.response.device);
                assert.equal('string', data.response.ip_address);
                assert.equal('string', data.response.location);
                assert.equal('string', data.response.join_time);
                assert.equal('string', data.response.leave_time);
                assert.equal('string', data.response.pc_name);
                assert.equal('string', data.response.domain);
                assert.equal('string', data.response.mac_addr);
                assert.equal('string', data.response.harddisk_id);
                assert.equal('string', data.response.version);
                assert.equal('object', typeof data.response.user_qos);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dashboards', 'dashboardWebinarParticipantQOS', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#dashboardZoomRooms - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.dashboardZoomRooms(null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(10, data.response.page_count);
                assert.equal(1, data.response.pageNumber);
                assert.equal(30, data.response.pageSize);
                assert.equal(7, data.response.total_records);
                assert.equal(true, Array.isArray(data.response.zoom_rooms));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dashboards', 'dashboardZoomRooms', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const dashboardsFrom = 'fakedata';
    const dashboardsTo = 'fakedata';
    const dashboardsZoomroomId = 'fakedata';
    describe('#dashboardZoomRoom - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.dashboardZoomRoom(dashboardsZoomroomId, dashboardsFrom, dashboardsTo, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.room_name);
                assert.equal('string', data.response.calender_name);
                assert.equal('string', data.response.email);
                assert.equal('string', data.response.account_type);
                assert.equal('string', data.response.status);
                assert.equal('string', data.response.device_ip);
                assert.equal('string', data.response.camera);
                assert.equal('string', data.response.microphone);
                assert.equal('string', data.response.speaker);
                assert.equal('string', data.response.last_start_time);
                assert.equal('object', typeof data.response.live_meeting);
                assert.equal('object', typeof data.response.past_meetings);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dashboards', 'dashboardZoomRoom', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#reportCloudRecording - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.reportCloudRecording('fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.from);
                assert.equal('string', data.response.to);
                assert.equal(true, Array.isArray(data.response.cloud_recording_storage));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Reports', 'reportCloudRecording', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#reportDaily - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.reportDaily(null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(8, data.response.year);
                assert.equal(8, data.response.month);
                assert.equal(true, Array.isArray(data.response.dates));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Reports', 'reportDaily', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const reportsMeetingId = 'fakedata';
    describe('#reportMeetingDetails - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.reportMeetingDetails(reportsMeetingId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.uuid);
                assert.equal(6, data.response.id);
                assert.equal(2, data.response.type);
                assert.equal('string', data.response.topic);
                assert.equal('string', data.response.user_name);
                assert.equal('string', data.response.user_email);
                assert.equal('string', data.response.start_time);
                assert.equal('string', data.response.end_time);
                assert.equal(1, data.response.duration);
                assert.equal(1, data.response.total_minutes);
                assert.equal(7, data.response.participants_count);
                assert.equal(true, Array.isArray(data.response.tracking_fields));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Reports', 'reportMeetingDetails', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#reportMeetingParticipants - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.reportMeetingParticipants(reportsMeetingId, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(10, data.response.page_count);
                assert.equal(30, data.response.pageSize);
                assert.equal(2, data.response.total_records);
                assert.equal('string', data.response.nextPageToken);
                assert.equal(true, Array.isArray(data.response.participants));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Reports', 'reportMeetingParticipants', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#reportMeetingPolls - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.reportMeetingPolls(reportsMeetingId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(8, data.response.id);
                assert.equal('string', data.response.uuid);
                assert.equal('string', data.response.start_time);
                assert.equal(true, Array.isArray(data.response.questions));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Reports', 'reportMeetingPolls', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#reportTelephone - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.reportTelephone(null, 'fakedata', 'fakedata', null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.from);
                assert.equal('string', data.response.to);
                assert.equal(8, data.response.page_count);
                assert.equal(1, data.response.pageNumber);
                assert.equal(30, data.response.pageSize);
                assert.equal(2, data.response.total_records);
                assert.equal(true, Array.isArray(data.response.telephony_usage));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Reports', 'reportTelephone', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#reportUsers - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.reportUsers(null, 'fakedata', 'fakedata', null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.from);
                assert.equal('string', data.response.to);
                assert.equal(3, data.response.page_count);
                assert.equal(1, data.response.pageNumber);
                assert.equal(30, data.response.pageSize);
                assert.equal(4, data.response.total_records);
                assert.equal(5, data.response.total_meetings);
                assert.equal(2, data.response.total_participants);
                assert.equal(1, data.response.total_meeting_minutes);
                assert.equal(true, Array.isArray(data.response.users));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Reports', 'reportUsers', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const reportsUserId = 'fakedata';
    describe('#reportMeetings - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.reportMeetings(reportsUserId, 'fakedata', 'fakedata', null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.from);
                assert.equal('string', data.response.to);
                assert.equal(9, data.response.page_count);
                assert.equal(30, data.response.pageSize);
                assert.equal(5, data.response.total_records);
                assert.equal('string', data.response.nextPageToken);
                assert.equal(true, Array.isArray(data.response.meetings));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Reports', 'reportMeetings', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const reportsWebinarId = 'fakedata';
    describe('#reportWebinarDetails - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.reportWebinarDetails(reportsWebinarId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.uuid);
                assert.equal(5, data.response.id);
                assert.equal(7, data.response.type);
                assert.equal('string', data.response.topic);
                assert.equal('string', data.response.user_name);
                assert.equal('string', data.response.user_email);
                assert.equal('string', data.response.start_time);
                assert.equal('string', data.response.end_time);
                assert.equal(4, data.response.duration);
                assert.equal(3, data.response.total_minutes);
                assert.equal(6, data.response.participants_count);
                assert.equal(true, Array.isArray(data.response.tracking_fields));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Reports', 'reportWebinarDetails', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#reportWebinarParticipants - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.reportWebinarParticipants(reportsWebinarId, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(7, data.response.page_count);
                assert.equal(30, data.response.pageSize);
                assert.equal(9, data.response.total_records);
                assert.equal('string', data.response.nextPageToken);
                assert.equal(true, Array.isArray(data.response.participants));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Reports', 'reportWebinarParticipants', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#reportWebinarPolls - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.reportWebinarPolls(reportsWebinarId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(4, data.response.id);
                assert.equal('string', data.response.uuid);
                assert.equal('string', data.response.start_time);
                assert.equal(true, Array.isArray(data.response.questions));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Reports', 'reportWebinarPolls', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#reportWebinarQA - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.reportWebinarQA(reportsWebinarId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(4, data.response.id);
                assert.equal('string', data.response.uuid);
                assert.equal('string', data.response.start_time);
                assert.equal(true, Array.isArray(data.response.questions));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Reports', 'reportWebinarQA', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const tSPUserId = 'fakedata';
    const tSPUserTSPCreateBodyParam = {
      conference_code: 'string',
      leader_pin: 'string'
    };
    describe('#userTSPCreate - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.userTSPCreate(tSPUserId, tSPUserTSPCreateBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.conference_code);
                assert.equal('string', data.response.leader_pin);
                assert.equal(true, Array.isArray(data.response.dial_in_numbers));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('TSP', 'userTSPCreate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const tSPPatchTspBodyParam = {
      tsp_provider: 'string',
      enable: false
    };
    describe('#patchTsp - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.patchTsp(tSPPatchTspBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-zoom-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('TSP', 'patchTsp', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTsp - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getTsp((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.tsp_provider);
                assert.equal(true, data.response.enable);
                assert.equal(true, Array.isArray(data.response.dial_in_numbers));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('TSP', 'getTsp', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#userTSPs - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.userTSPs(tSPUserId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.tsp_accounts));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('TSP', 'userTSPs', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const tSPTspId = 'fakedata';
    const tSPUserTSPUpdateBodyParam = {
      conference_code: 'string',
      leader_pin: 'string'
    };
    describe('#userTSPUpdate - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.userTSPUpdate(tSPUserId, tSPTspId, tSPUserTSPUpdateBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-zoom-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('TSP', 'userTSPUpdate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#userTSP - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.userTSP(tSPUserId, tSPTspId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.conference_code);
                assert.equal('string', data.response.leader_pin);
                assert.equal(true, Array.isArray(data.response.dial_in_numbers));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('TSP', 'userTSP', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    let usersEmail = 'fakedata';
    let usersUserId = 'fakedata';
    let usersAssistantId = 'fakedata';
    let usersSchedulerId = 'fakedata';
    const usersUserCreateBodyParam = {
      action: 'ssoCreate'
    };
    describe('#userCreate - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.userCreate(usersUserCreateBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.first_name);
                assert.equal('string', data.response.last_name);
                assert.equal('string', data.response.email);
                assert.equal(2, data.response.type);
              } else {
                runCommonAsserts(data, error);
              }
              usersEmail = data.response.email;
              usersUserId = data.response.id;
              usersAssistantId = data.response.id;
              usersSchedulerId = data.response.id;
              saveMockData('Users', 'userCreate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const usersUserAssistantCreateBodyParam = {
      assistants: [
        {
          id: 'string',
          email: 'string'
        }
      ]
    };
    describe('#userAssistantCreate - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.userAssistantCreate(usersUserId, usersUserAssistantCreateBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.ids);
                assert.equal('string', data.response.add_at);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Users', 'userAssistantCreate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const usersPicFile = 'fakedata';
    describe('#userPicture - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.userPicture(usersUserId, usersPicFile, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-zoom-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Users', 'userPicture', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getUsers - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getUsers(null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(5, data.response.page_count);
                assert.equal(1, data.response.pageNumber);
                assert.equal(30, data.response.pageSize);
                assert.equal(2, data.response.total_records);
                assert.equal(true, Array.isArray(data.response.users));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Users', 'getUsers', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#userEmail - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.userEmail(usersEmail, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(false, data.response.existed_email);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Users', 'userEmail', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const usersVanityName = 'fakedata';
    describe('#userVanityName - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.userVanityName(usersVanityName, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, data.response.existed);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Users', 'userVanityName', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const usersZpk = 'fakedata';
    describe('#userZPK - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.userZPK(usersZpk, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(3, data.response.expire_in);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Users', 'userZPK', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const usersUserUpdateBodyParam = {
      first_name: 'string',
      last_name: 'string',
      type: 2,
      pmi: 'string',
      use_pmi: true,
      timezone: 'string',
      language: 'string',
      dept: 'string',
      vanity_name: 'string',
      host_key: 'string',
      cms_user_id: 'string'
    };
    describe('#userUpdate - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.userUpdate(usersUserId, usersUserUpdateBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-zoom-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Users', 'userUpdate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#user - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.user(usersUserId, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.first_name);
                assert.equal('string', data.response.last_name);
                assert.equal('string', data.response.email);
                assert.equal(3, data.response.type);
                assert.equal('string', data.response.pmi);
                assert.equal('string', data.response.timezone);
                assert.equal('string', data.response.dept);
                assert.equal('string', data.response.created_at);
                assert.equal('string', data.response.last_login_time);
                assert.equal('string', data.response.last_client_version);
                assert.equal('string', data.response.language);
                assert.equal('string', data.response.vanity_url);
                assert.equal('string', data.response.personal_meeting_url);
                assert.equal(8, data.response.verified);
                assert.equal('string', data.response.pic_url);
                assert.equal('string', data.response.cms_user_id);
                assert.equal('string', data.response.account_id);
                assert.equal('string', data.response.host_key);
                assert.equal(false, data.response.use_pmi);
                assert.equal(true, Array.isArray(data.response.group_ids));
                assert.equal(true, Array.isArray(data.response.im_group_ids));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Users', 'user', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#userAssistants - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.userAssistants(usersUserId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.assistants));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Users', 'userAssistants', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const usersUserEmailUpdateBodyParam = {
      email: 'string'
    };
    describe('#userEmailUpdate - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.userEmailUpdate(usersUserId, usersUserEmailUpdateBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-zoom-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Users', 'userEmailUpdate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const usersUserPasswordBodyParam = {
      password: 'string'
    };
    describe('#userPassword - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.userPassword(usersUserId, usersUserPasswordBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-zoom-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Users', 'userPassword', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#userPermission - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.userPermission(usersUserId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.permissions));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Users', 'userPermission', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#userSchedulers - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.userSchedulers(usersUserId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.assistants));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Users', 'userSchedulers', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const usersUserSettingsUpdateBodyParam = {
      schedule_meeting: {
        host_video: false,
        participants_video: false,
        audio_type: 'voip',
        join_before_host: false,
        force_pmi_jbh_password: true,
        pstn_password_protected: true
      },
      in_meeting: {
        e2e_encryption: true,
        chat: false,
        private_chat: true,
        auto_saving_chat: true,
        entry_exit_chime: 'a\'\'',
        record_play_voice: true,
        file_transfer: false,
        feedback: false,
        co_host: false,
        polling: false,
        attendee_on_hold: true,
        annotation: true,
        remote_control: true,
        non_verbal_feedback: false,
        breakout_room: true,
        remote_support: false,
        closed_caption: false,
        group_hd: false,
        virtual_background: true,
        far_end_camera_control: true,
        share_dual_camera: true,
        attention_tracking: false,
        waiting_room: true,
        allow_live_streaming: false,
        workplace_by_facebook: true,
        custom_live_streaming: false,
        custom_service_instructions: 'string'
      },
      email_notification: {
        jbh_reminder: true,
        cancel_meeting_reminder: true,
        alternative_host_reminder: false
      },
      recording: {
        local_recording: true,
        cloud_recording: false,
        record_speaker_view: true,
        record_gallery_view: false,
        record_audio_file: true,
        save_chat_text: true,
        show_timestamp: false,
        recording_audio_transcript: false,
        auto_recording: 'local',
        auto_delete_cmr: false,
        auto_delete_cmr_days: 9
      },
      telephony: {
        third_party_audio: false,
        audio_conference_info: 'string',
        show_international_numbers_link: false
      },
      feature: {
        meeting_capacity: 3,
        large_meeting: true,
        large_meeting_capacity: 3,
        webinar: true,
        webinar_capacity: 8
      }
    };
    describe('#userSettingsUpdate - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.userSettingsUpdate(usersUserId, usersUserSettingsUpdateBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-zoom-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Users', 'userSettingsUpdate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#userSettings - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.userSettings(usersUserId, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.schedule_meeting);
                assert.equal('object', typeof data.response.in_meeting);
                assert.equal('object', typeof data.response.email_notification);
                assert.equal('object', typeof data.response.recording);
                assert.equal('object', typeof data.response.telephony);
                assert.equal('object', typeof data.response.feature);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Users', 'userSettings', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const usersUserStatusBodyParam = {
      action: 'activate'
    };
    describe('#userStatus - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.userStatus(usersUserId, usersUserStatusBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-zoom-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Users', 'userStatus', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#userToken - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.userToken(usersUserId, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.token);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Users', 'userToken', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const pACUserId = 'fakedata';
    describe('#userPACs - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.userPACs(pACUserId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.tsp_accounts));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('PAC', 'userPACs', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    let webinarsUserId = 'fakedata';
    const webinarsWebinarCreateBodyParam = {
      topic: 'string',
      type: 5,
      start_time: 'string',
      duration: 4,
      timezone: 'string',
      password: 'string',
      agenda: 'string',
      tracking_fields: [
        {
          field: 'string',
          value: 'string'
        }
      ],
      recurrence: {
        type: 3,
        repeat_interval: 5,
        weekly_days: 7,
        monthly_day: 1,
        monthly_week: -1,
        monthly_week_day: 4,
        end_times: 1,
        end_date_time: 'string'
      },
      settings: {
        host_video: false,
        panelists_video: true,
        practice_session: true,
        hd_video: false,
        approval_type: 2,
        registration_type: 1,
        audio: 'both',
        auto_recording: 'none',
        enforce_login: false,
        enforce_login_domains: 'string',
        alternative_hosts: 'string',
        close_registration: false,
        show_share_button: false,
        allow_multiple_devices: false,
        on_demand: false
      }
    };
    describe('#webinarCreate - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.webinarCreate(webinarsUserId, webinarsWebinarCreateBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.uuid);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.host_id);
                assert.equal('string', data.response.topic);
                assert.equal(5, data.response.type);
                assert.equal('string', data.response.start_time);
                assert.equal(2, data.response.duration);
                assert.equal('string', data.response.timezone);
                assert.equal('string', data.response.agenda);
                assert.equal('string', data.response.created_at);
                assert.equal('string', data.response.start_url);
                assert.equal('string', data.response.join_url);
                assert.equal(true, Array.isArray(data.response.tracking_fields));
                assert.equal(true, Array.isArray(data.response.occurrences));
                assert.equal('object', typeof data.response.settings);
              } else {
                runCommonAsserts(data, error);
              }
              webinarsUserId = data.response.id;
              saveMockData('Webinars', 'webinarCreate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    let webinarsWebinarId = 555;
    let webinarsPanelistId = 555;
    const webinarsWebinarPanelistCreateBodyParam = {
      panelists: [
        {
          name: 'string',
          email: 'string'
        }
      ]
    };
    describe('#webinarPanelistCreate - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.webinarPanelistCreate(webinarsWebinarId, webinarsWebinarPanelistCreateBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.join_url);
              } else {
                runCommonAsserts(data, error);
              }
              webinarsWebinarId = data.response.id;
              webinarsPanelistId = data.response.id;
              saveMockData('Webinars', 'webinarPanelistCreate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    let webinarsPollId = 'fakedata';
    const webinarsWebinarPollCreateBodyParam = {
      title: 'string',
      questions: [
        {
          name: 'string',
          type: 'multiple',
          answers: [
            'string'
          ]
        }
      ]
    };
    describe('#webinarPollCreate - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.webinarPollCreate(webinarsWebinarId, webinarsWebinarPollCreateBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.id);
                assert.equal('sharing', data.response.status);
                assert.equal('string', data.response.title);
                assert.equal(true, Array.isArray(data.response.questions));
              } else {
                runCommonAsserts(data, error);
              }
              webinarsPollId = data.response.id;
              saveMockData('Webinars', 'webinarPollCreate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const webinarsWebinarRegistrantCreateBodyParam = {
      email: 'string',
      first_name: 'string',
      last_name: 'string',
      address: 'string',
      city: 'string',
      country: 'string',
      zip: 'string',
      state: 'string',
      phone: 'string',
      industry: 'string',
      org: 'string',
      job_title: 'string',
      purchasing_time_frame: 'More than 6 months',
      role_in_purchase_process: 'Not involved',
      no_of_employees: '51-100',
      comments: 'string',
      custom_questions: [
        {
          title: 'string',
          value: 'string'
        }
      ]
    };
    describe('#webinarRegistrantCreate - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.webinarRegistrantCreate(webinarsWebinarId, null, webinarsWebinarRegistrantCreateBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.registrant_id);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.topic);
                assert.equal('string', data.response.start_time);
                assert.equal('string', data.response.join_url);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Webinars', 'webinarRegistrantCreate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#pastWebinars - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.pastWebinars(webinarsWebinarId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.webinars));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Webinars', 'pastWebinars', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#webinars - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.webinars(webinarsUserId, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(10, data.response.page_count);
                assert.equal(1, data.response.pageNumber);
                assert.equal(30, data.response.pageSize);
                assert.equal(9, data.response.total_records);
                assert.equal(true, Array.isArray(data.response.webinars));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Webinars', 'webinars', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const webinarsWebinarUpdateBodyParam = {
      topic: 'string',
      type: 5,
      start_time: 'string',
      duration: 2,
      timezone: 'string',
      password: 'string',
      agenda: 'string',
      tracking_fields: [
        {
          field: 'string',
          value: 'string'
        }
      ],
      recurrence: {
        type: 3,
        repeat_interval: 6,
        weekly_days: 5,
        monthly_day: 10,
        monthly_week: -1,
        monthly_week_day: 2,
        end_times: 1,
        end_date_time: 'string'
      },
      settings: {
        host_video: false,
        panelists_video: true,
        practice_session: true,
        hd_video: true,
        approval_type: 2,
        registration_type: 1,
        audio: 'both',
        auto_recording: 'none',
        enforce_login: true,
        enforce_login_domains: 'string',
        alternative_hosts: 'string',
        close_registration: false,
        show_share_button: true,
        allow_multiple_devices: true,
        on_demand: false,
        registrants_confirmation_email: true
      }
    };
    describe('#webinarUpdate - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.webinarUpdate(webinarsWebinarId, webinarsWebinarUpdateBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-zoom-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Webinars', 'webinarUpdate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#webinar - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.webinar(webinarsWebinarId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.uuid);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.host_id);
                assert.equal('string', data.response.topic);
                assert.equal(5, data.response.type);
                assert.equal('string', data.response.start_time);
                assert.equal(10, data.response.duration);
                assert.equal('string', data.response.timezone);
                assert.equal('string', data.response.agenda);
                assert.equal('string', data.response.created_at);
                assert.equal('string', data.response.start_url);
                assert.equal('string', data.response.join_url);
                assert.equal(true, Array.isArray(data.response.tracking_fields));
                assert.equal(true, Array.isArray(data.response.occurrences));
                assert.equal('object', typeof data.response.settings);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Webinars', 'webinar', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#webinarPanelists - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.webinarPanelists(webinarsWebinarId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(2, data.response.total_records);
                assert.equal(true, Array.isArray(data.response.panelists));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Webinars', 'webinarPanelists', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#webinarPolls - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.webinarPolls(webinarsWebinarId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(9, data.response.total_records);
                assert.equal(true, Array.isArray(data.response.polls));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Webinars', 'webinarPolls', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const webinarsWebinarPollUpdateBodyParam = {
      title: 'string',
      questions: [
        {
          name: 'string',
          type: 'multiple',
          answers: [
            'string'
          ]
        }
      ]
    };
    describe('#webinarPollUpdate - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.webinarPollUpdate(webinarsWebinarId, webinarsPollId, webinarsWebinarPollUpdateBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-zoom-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Webinars', 'webinarPollUpdate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#webinarPollGet - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.webinarPollGet(webinarsWebinarId, webinarsPollId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.id);
                assert.equal('notstart', data.response.status);
                assert.equal('string', data.response.title);
                assert.equal(true, Array.isArray(data.response.questions));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Webinars', 'webinarPollGet', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#webinarRegistrants - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.webinarRegistrants(webinarsWebinarId, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(7, data.response.page_count);
                assert.equal(1, data.response.pageNumber);
                assert.equal(30, data.response.pageSize);
                assert.equal(5, data.response.total_records);
                assert.equal(true, Array.isArray(data.response.registrants));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Webinars', 'webinarRegistrants', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const webinarsWebinarRegistrantStatusBodyParam = {
      action: 'approve'
    };
    describe('#webinarRegistrantStatus - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.webinarRegistrantStatus(webinarsWebinarId, null, webinarsWebinarRegistrantStatusBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-zoom-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Webinars', 'webinarRegistrantStatus', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const webinarsWebinarStatusBodyParam = {
      action: 'end'
    };
    describe('#webinarStatus - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.webinarStatus(webinarsWebinarId, webinarsWebinarStatusBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-zoom-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Webinars', 'webinarStatus', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const webhooksWebhookCreateBodyParam = {
      url: 'string',
      auth_user: 'string',
      auth_password: 'string',
      events: 'recording_completed'
    };
    describe('#webhookCreate - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.webhookCreate(webhooksWebhookCreateBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.webhook_id);
                assert.equal('string', data.response.url);
                assert.equal('string', data.response.auth_user);
                assert.equal('string', data.response.auth_password);
                assert.equal('recording_completed', data.response.events);
                assert.equal('string', data.response.created_at);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Webhooks', 'webhookCreate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebhooks - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getWebhooks((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(9, data.response.total_records);
                assert.equal(true, Array.isArray(data.response.webhooks));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Webhooks', 'getWebhooks', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const webhooksWebhookSwitchBodyParam = {
      version: 'v1'
    };
    describe('#webhookSwitch - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.webhookSwitch(webhooksWebhookSwitchBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-zoom-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Webhooks', 'webhookSwitch', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const webhooksWebhookId = 'fakedata';
    const webhooksWebhookUpdateBodyParam = {
      url: 'string',
      auth_user: 'string',
      auth_password: 'string',
      events: 'recording_completed'
    };
    describe('#webhookUpdate - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.webhookUpdate(webhooksWebhookId, webhooksWebhookUpdateBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-zoom-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Webhooks', 'webhookUpdate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#webhook - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.webhook(webhooksWebhookId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.webhook_id);
                assert.equal('string', data.response.url);
                assert.equal('string', data.response.auth_user);
                assert.equal('string', data.response.auth_password);
                assert.equal('meeting_ended', data.response.events);
                assert.equal('string', data.response.created_at);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Webhooks', 'webhook', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#accountDisassociate - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.accountDisassociate(accountsAccountId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-zoom-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Accounts', 'accountDisassociate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#groupDelete - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.groupDelete(groupsGroupId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-zoom-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Groups', 'groupDelete', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#groupMembersDelete - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.groupMembersDelete(groupsGroupId, groupsMemberId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-zoom-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Groups', 'groupMembersDelete', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deviceDelete - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deviceDelete(devicesDeviceId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-zoom-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Devices', 'deviceDelete', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#trackingfieldDelete - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.trackingfieldDelete(trackingFieldFieldId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-zoom-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('TrackingField', 'trackingfieldDelete', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#imGroupDelete - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.imGroupDelete(iMGroupsGroupId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-zoom-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('IMGroups', 'imGroupDelete', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#imGroupMembersDelete - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.imGroupMembersDelete(iMGroupsGroupId, iMGroupsMemberId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-zoom-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('IMGroups', 'imGroupMembersDelete', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#meetingDelete - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.meetingDelete(meetingsMeetingId, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-zoom-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Meetings', 'meetingDelete', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#meetingPollDelete - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.meetingPollDelete(meetingsMeetingId, meetingsPollId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-zoom-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Meetings', 'meetingPollDelete', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#recordingDelete - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.recordingDelete(cloudRecordingMeetingId, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-zoom-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CloudRecording', 'recordingDelete', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#recordingDeleteOne - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.recordingDeleteOne(cloudRecordingMeetingId, cloudRecordingRecordingId, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-zoom-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CloudRecording', 'recordingDeleteOne', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#userTSPDelete - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.userTSPDelete(tSPUserId, tSPTspId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-zoom-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('TSP', 'userTSPDelete', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#userDelete - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.userDelete(usersUserId, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-zoom-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Users', 'userDelete', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#userAssistantsDelete - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.userAssistantsDelete(usersUserId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-zoom-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Users', 'userAssistantsDelete', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#userAssistantDelete - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.userAssistantDelete(usersUserId, usersAssistantId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-zoom-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Users', 'userAssistantDelete', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#userSchedulersDelete - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.userSchedulersDelete(usersUserId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-zoom-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Users', 'userSchedulersDelete', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#userSchedulerDelete - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.userSchedulerDelete(usersUserId, usersSchedulerId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-zoom-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Users', 'userSchedulerDelete', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#userSSOTokenDelete - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.userSSOTokenDelete(usersUserId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-zoom-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Users', 'userSSOTokenDelete', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#webinarDelete - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.webinarDelete(webinarsWebinarId, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-zoom-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Webinars', 'webinarDelete', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#webinarPanelistsDelete - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.webinarPanelistsDelete(webinarsWebinarId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-zoom-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Webinars', 'webinarPanelistsDelete', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#webinarPanelistDelete - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.webinarPanelistDelete(webinarsWebinarId, webinarsPanelistId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-zoom-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Webinars', 'webinarPanelistDelete', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#webinarPollDelete - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.webinarPollDelete(webinarsWebinarId, webinarsPollId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-zoom-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Webinars', 'webinarPollDelete', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#webhookDelete - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.webhookDelete(webhooksWebhookId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-zoom-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Webhooks', 'webhookDelete', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });
  });
});
