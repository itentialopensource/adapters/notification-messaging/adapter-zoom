## Using this Adapter

The `adapter.js` file contains the calls the adapter makes available to the rest of the Itential Platform. The API detailed for these calls should be available through JSDOC. The following is a brief summary of the calls.

### Generic Adapter Calls

These are adapter methods that IAP or you might use. There are some other methods not shown here that might be used for internal adapter functionality.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">connect()</td>
    <td style="padding:15px">This call is run when the Adapter is first loaded by he Itential Platform. It validates the properties have been provided correctly.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">healthCheck(callback)</td>
    <td style="padding:15px">This call ensures that the adapter can communicate with Adapter for Zoom. The actual call that is used is defined in the adapter properties and .system entities action.json file.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">refreshProperties(properties)</td>
    <td style="padding:15px">This call provides the adapter the ability to accept property changes without having to restart the adapter.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">encryptProperty(property, technique, callback)</td>
    <td style="padding:15px">This call will take the provided property and technique, and return the property encrypted with the technique. This allows the property to be used in the adapterProps section for the credential password so that the password does not have to be in clear text. The adapter will decrypt the property as needed for communications with Adapter for Zoom.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">iapUpdateAdapterConfiguration(configFile, changes, entity, type, action, callback)</td>
    <td style="padding:15px">This call provides the ability to update the adapter configuration from IAP - includes actions, schema, mockdata and other configurations.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapSuspendAdapter(mode, callback)</td>
    <td style="padding:15px">This call provides the ability to suspend the adapter and either have requests rejected or put into a queue to be processed after the adapter is resumed.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapUnsuspendAdapter(callback)</td>
    <td style="padding:15px">This call provides the ability to resume a suspended adapter. Any requests in queue will be processed before new requests.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetAdapterQueue(callback)</td>
    <td style="padding:15px">This call will return the requests that are waiting in the queue if throttling is enabled.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapFindAdapterPath(apiPath, callback)</td>
    <td style="padding:15px">This call provides the ability to see if a particular API path is supported by the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapTroubleshootAdapter(props, persistFlag, adapter, callback)</td>
    <td style="padding:15px">This call can be used to check on the performance of the adapter - it checks connectivity, healthcheck and basic get calls.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterHealthcheck(adapter, callback)</td>
    <td style="padding:15px">This call will return the results of a healthcheck.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterConnectivity(callback)</td>
    <td style="padding:15px">This call will return the results of a connectivity check.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterBasicGet(callback)</td>
    <td style="padding:15px">This call will return the results of running basic get API calls.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapMoveAdapterEntitiesToDB(callback)</td>
    <td style="padding:15px">This call will push the adapter configuration from the entities directory into the Adapter or IAP Database.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapDeactivateTasks(tasks, callback)</td>
    <td style="padding:15px">This call provides the ability to remove tasks from the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapActivateTasks(tasks, callback)</td>
    <td style="padding:15px">This call provides the ability to add deactivated tasks back into the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapExpandedGenericAdapterRequest(metadata, uriPath, restMethod, pathVars, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This is an expanded Generic Call. The metadata object allows us to provide many new capabilities within the generic request.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">genericAdapterRequest(uriPath, restMethod, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This call allows you to provide the path to have the adapter call. It is an easy way to incorporate paths that have not been built into the adapter yet.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">genericAdapterRequestNoBasePath(uriPath, restMethod, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This call is the same as the genericAdapterRequest only it does not add a base_path or version to the call.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterLint(callback)</td>
    <td style="padding:15px">Runs lint on the addapter and provides the information back.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterTests(callback)</td>
    <td style="padding:15px">Runs baseunit and unit tests on the adapter and provides the information back.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetAdapterInventory(callback)</td>
    <td style="padding:15px">This call provides some inventory related information about the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
  
### Adapter Cache Calls

These are adapter methods that are used for adapter caching. If configured, the adapter will cache based on the interval provided. However, you can force a population of the cache manually as well.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">iapPopulateEntityCache(entityTypes, callback)</td>
    <td style="padding:15px">This call populates the adapter cache.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRetrieveEntitiesCache(entityType, options, callback)</td>
    <td style="padding:15px">This call retrieves the specific items from the adapter cache.</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
  
### Adapter Broker Calls

These are adapter methods that are used to integrate to IAP Brokers. This adapter currently supports the following broker calls.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">hasEntities(entityType, entityList, callback)</td>
    <td style="padding:15px">This call is utilized by the IAP Device Broker to determine if the adapter has a specific entity and item of the entity.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevice(deviceName, callback)</td>
    <td style="padding:15px">This call returns the details of the requested device.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevicesFiltered(options, callback)</td>
    <td style="padding:15px">This call returns the list of devices that match the criteria provided in the options filter.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">isAlive(deviceName, callback)</td>
    <td style="padding:15px">This call returns whether the device status is active</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfig(deviceName, format, callback)</td>
    <td style="padding:15px">This call returns the configuration for the selected device.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetDeviceCount(callback)</td>
    <td style="padding:15px">This call returns the count of devices.</td>
    <td style="padding:15px">No</td>
  </tr>
</table>
<br>

### Specific Adapter Calls

Specific adapter calls are built based on the API of the Zoom. The Adapter Builder creates the proper method comments for generating JS-DOC for the adapter. This is the best way to get information on the calls.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Path</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">getAccounts(pageSize, pageNumber, callback)</td>
    <td style="padding:15px">List sub accounts</td>
    <td style="padding:15px">{base_path}/{version}/accounts?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">accountCreate(body, callback)</td>
    <td style="padding:15px">Create a sub account</td>
    <td style="padding:15px">{base_path}/{version}/accounts?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">account(accountId, callback)</td>
    <td style="padding:15px">Retrieve a sub account</td>
    <td style="padding:15px">{base_path}/{version}/accounts/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">accountDisassociate(accountId, callback)</td>
    <td style="padding:15px">Disassociate an account</td>
    <td style="padding:15px">{base_path}/{version}/accounts/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">accountOptionsUpdate(accountId, body, callback)</td>
    <td style="padding:15px">Update a sub account's options</td>
    <td style="padding:15px">{base_path}/{version}/accounts/{pathv1}/options?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">accountSettings(accountId, callback)</td>
    <td style="padding:15px">Retrieve a sub account's settings</td>
    <td style="padding:15px">{base_path}/{version}/accounts/{pathv1}/settings?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">accountSettingsUpdate(accountId, body, callback)</td>
    <td style="padding:15px">Update a sub account's settings</td>
    <td style="padding:15px">{base_path}/{version}/accounts/{pathv1}/settings?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">accountManagedDomain(accountId, callback)</td>
    <td style="padding:15px">Retrieve a sub account's managed domains</td>
    <td style="padding:15px">{base_path}/{version}/accounts/{pathv1}/managed_domains?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">accountBilling(accountId, callback)</td>
    <td style="padding:15px">Retrieve billing information for a sub account</td>
    <td style="padding:15px">{base_path}/{version}/accounts/{pathv1}/billing?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">accountBillingUpdate(accountId, body, callback)</td>
    <td style="padding:15px">Update billing information for a sub account</td>
    <td style="padding:15px">{base_path}/{version}/accounts/{pathv1}/billing?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">accountPlans(accountId, callback)</td>
    <td style="padding:15px">Retrieve plan information for a sub account</td>
    <td style="padding:15px">{base_path}/{version}/accounts/{pathv1}/plans?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">accountPlanCreate(accountId, body, callback)</td>
    <td style="padding:15px">Subscribe plans for a sub account</td>
    <td style="padding:15px">{base_path}/{version}/accounts/{pathv1}/plans?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">accountPlanBaseUpdate(accountId, body, callback)</td>
    <td style="padding:15px">Update a base plan for a sub account</td>
    <td style="padding:15px">{base_path}/{version}/accounts/{pathv1}/plans/base?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">accountPlanAddonCreate(accountId, body, callback)</td>
    <td style="padding:15px">Add an additional plan for sub account</td>
    <td style="padding:15px">{base_path}/{version}/accounts/{pathv1}/plans/addons?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">accountPlanAddonUpdate(accountId, body, callback)</td>
    <td style="padding:15px">Update an additional plan for sub account</td>
    <td style="padding:15px">{base_path}/{version}/accounts/{pathv1}/plans/addons?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getGroups(callback)</td>
    <td style="padding:15px">List groups</td>
    <td style="padding:15px">{base_path}/{version}/groups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">groupCreate(body, callback)</td>
    <td style="padding:15px">Create a group</td>
    <td style="padding:15px">{base_path}/{version}/groups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">group(groupId, callback)</td>
    <td style="padding:15px">Retrieve a group</td>
    <td style="padding:15px">{base_path}/{version}/groups/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">groupUpdate(groupId, body, callback)</td>
    <td style="padding:15px">Update a group</td>
    <td style="padding:15px">{base_path}/{version}/groups/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">groupDelete(groupId, callback)</td>
    <td style="padding:15px">Delete a group</td>
    <td style="padding:15px">{base_path}/{version}/groups/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">groupMembers(groupId, pageSize, pageNumber, callback)</td>
    <td style="padding:15px">List a group's members</td>
    <td style="padding:15px">{base_path}/{version}/groups/{pathv1}/members?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">groupMembersCreate(groupId, body, callback)</td>
    <td style="padding:15px">Add group members</td>
    <td style="padding:15px">{base_path}/{version}/groups/{pathv1}/members?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">groupMembersDelete(groupId, memberId, callback)</td>
    <td style="padding:15px">Delete a group member</td>
    <td style="padding:15px">{base_path}/{version}/groups/{pathv1}/members/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deviceList(callback)</td>
    <td style="padding:15px">List H.323/SIP Devices.</td>
    <td style="padding:15px">{base_path}/{version}/h323/devices?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deviceCreate(body, callback)</td>
    <td style="padding:15px">Create a H.323/SIP Device</td>
    <td style="padding:15px">{base_path}/{version}/h323/devices?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deviceUpdate(deviceId, body, callback)</td>
    <td style="padding:15px">Update a H.323/SIP Device</td>
    <td style="padding:15px">{base_path}/{version}/h323/devices/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deviceDelete(deviceId, callback)</td>
    <td style="padding:15px">Delete a H.323/SIP Device</td>
    <td style="padding:15px">{base_path}/{version}/h323/devices/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">trackingfieldList(callback)</td>
    <td style="padding:15px">List Tracking Fields.</td>
    <td style="padding:15px">{base_path}/{version}/tracking_fields?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">trackingfieldCreate(body, callback)</td>
    <td style="padding:15px">Create a Tracking Field</td>
    <td style="padding:15px">{base_path}/{version}/tracking_fields?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">trackingfieldGet(fieldId, callback)</td>
    <td style="padding:15px">Retrieve a tracking field</td>
    <td style="padding:15px">{base_path}/{version}/tracking_fields/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">trackingfieldUpdate(fieldId, body, callback)</td>
    <td style="padding:15px">Update a Tracking Field</td>
    <td style="padding:15px">{base_path}/{version}/tracking_fields/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">trackingfieldDelete(fieldId, callback)</td>
    <td style="padding:15px">Delete a Tracking Field</td>
    <td style="padding:15px">{base_path}/{version}/tracking_fields/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">imGroups(callback)</td>
    <td style="padding:15px">List IM Groups</td>
    <td style="padding:15px">{base_path}/{version}/im/groups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">imGroupCreate(body, callback)</td>
    <td style="padding:15px">Create an IM Group</td>
    <td style="padding:15px">{base_path}/{version}/im/groups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">imGroup(groupId, callback)</td>
    <td style="padding:15px">Retrieve an IM Group</td>
    <td style="padding:15px">{base_path}/{version}/im/groups/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">imGroupUpdate(groupId, body, callback)</td>
    <td style="padding:15px">Update an IM Group</td>
    <td style="padding:15px">{base_path}/{version}/im/groups/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">imGroupDelete(groupId, callback)</td>
    <td style="padding:15px">Delete an IM Group</td>
    <td style="padding:15px">{base_path}/{version}/im/groups/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">imGroupMembers(groupId, pageSize, pageNumber, callback)</td>
    <td style="padding:15px">List an IM Group's members</td>
    <td style="padding:15px">{base_path}/{version}/im/groups/{pathv1}/members?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">imGroupMembersCreate(groupId, body, callback)</td>
    <td style="padding:15px">Add IM Group members</td>
    <td style="padding:15px">{base_path}/{version}/im/groups/{pathv1}/members?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">imGroupMembersDelete(groupId, memberId, callback)</td>
    <td style="padding:15px">Delete an IM Group member</td>
    <td style="padding:15px">{base_path}/{version}/im/groups/{pathv1}/members/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">imChatSessions(from, to, pageSize, nextPageToken, callback)</td>
    <td style="padding:15px">Retrieve IM Chat sessions</td>
    <td style="padding:15px">{base_path}/{version}/im/chat/sessions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">imChatMessages(sessionId, from, to, pageSize, nextPageToken, callback)</td>
    <td style="padding:15px">Retrieve IM Chat messages</td>
    <td style="padding:15px">{base_path}/{version}/im/chat/sessions/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">meetings(userId, type = 'scheduled', pageSize, pageNumber, callback)</td>
    <td style="padding:15px">List meetings</td>
    <td style="padding:15px">{base_path}/{version}/users/{pathv1}/meetings?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">meetingCreate(userId, body, callback)</td>
    <td style="padding:15px">Create a meeting</td>
    <td style="padding:15px">{base_path}/{version}/users/{pathv1}/meetings?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">meeting(meetingId, callback)</td>
    <td style="padding:15px">Retrieve a meeting</td>
    <td style="padding:15px">{base_path}/{version}/meetings/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">meetingUpdate(meetingId, body, callback)</td>
    <td style="padding:15px">Update a meeting</td>
    <td style="padding:15px">{base_path}/{version}/meetings/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">meetingDelete(meetingId, occurrenceId, callback)</td>
    <td style="padding:15px">Delete a meeting</td>
    <td style="padding:15px">{base_path}/{version}/meetings/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">meetingStatus(meetingId, body, callback)</td>
    <td style="padding:15px">Update a meeting's status</td>
    <td style="padding:15px">{base_path}/{version}/meetings/{pathv1}/status?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">meetingInvitation(meetingId, callback)</td>
    <td style="padding:15px">Retrieve meeting invitation</td>
    <td style="padding:15px">{base_path}/{version}/meetings/{pathv1}/invitation?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">meetingRegistrants(meetingId, occurrenceId, status = 'pending', pageSize, pageNumber, callback)</td>
    <td style="padding:15px">List a meeting's registrants</td>
    <td style="padding:15px">{base_path}/{version}/meetings/{pathv1}/registrants?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">meetingRegistrantCreate(meetingId, occurrenceIds, body, callback)</td>
    <td style="padding:15px">Add a meeting registrant</td>
    <td style="padding:15px">{base_path}/{version}/meetings/{pathv1}/registrants?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">meetingRegistrantStatus(meetingId, occurrenceId, body, callback)</td>
    <td style="padding:15px">Update a meeting registrant's status</td>
    <td style="padding:15px">{base_path}/{version}/meetings/{pathv1}/registrants/status?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">meetingLiveStreamUpdate(meetingId, body, callback)</td>
    <td style="padding:15px">Update a meeting live stream</td>
    <td style="padding:15px">{base_path}/{version}/meetings/{pathv1}/livestream?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">meetingLiveStreamStatusUpdate(meetingId, body, callback)</td>
    <td style="padding:15px">Update a meeting live stream status</td>
    <td style="padding:15px">{base_path}/{version}/meetings/{pathv1}/livestream/status?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">pastMeetingDetails(meetingUUID, callback)</td>
    <td style="padding:15px">Retrieve past meeting details</td>
    <td style="padding:15px">{base_path}/{version}/past_meetings/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">pastMeetingParticipants(meetingUUID, pageSize, nextPageToken, callback)</td>
    <td style="padding:15px">Retrieve past meeting participants</td>
    <td style="padding:15px">{base_path}/{version}/past_meetings/{pathv1}/participants?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">pastMeetings(meetingId, callback)</td>
    <td style="padding:15px">List of ended meeting instances</td>
    <td style="padding:15px">{base_path}/{version}/past_meetings/{pathv1}/instances?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">meetingPolls(meetingId, callback)</td>
    <td style="padding:15px">List a meeting's polls</td>
    <td style="padding:15px">{base_path}/{version}/meetings/{pathv1}/polls?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">meetingPollCreate(meetingId, body, callback)</td>
    <td style="padding:15px">Create a meeting's poll</td>
    <td style="padding:15px">{base_path}/{version}/meetings/{pathv1}/polls?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">meetingPollGet(meetingId, pollId, callback)</td>
    <td style="padding:15px">Retrieve a meeting's poll</td>
    <td style="padding:15px">{base_path}/{version}/meetings/{pathv1}/polls/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">meetingPollUpdate(meetingId, pollId, body, callback)</td>
    <td style="padding:15px">Update a meeting's poll</td>
    <td style="padding:15px">{base_path}/{version}/meetings/{pathv1}/polls/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">meetingPollDelete(meetingId, pollId, callback)</td>
    <td style="padding:15px">Delete a meeting's Poll</td>
    <td style="padding:15px">{base_path}/{version}/meetings/{pathv1}/polls/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">recordingsList(userId, from, to, pageSize, nextPageToken, mc, trash, callback)</td>
    <td style="padding:15px">List all the recordings</td>
    <td style="padding:15px">{base_path}/{version}/users/{pathv1}/recordings?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">recordingGet(meetingId, callback)</td>
    <td style="padding:15px">Retrieve a meeting’s all recordings</td>
    <td style="padding:15px">{base_path}/{version}/meetings/{pathv1}/recordings?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">recordingDelete(meetingId, action = 'trash', callback)</td>
    <td style="padding:15px">Delete a meeting's recordings</td>
    <td style="padding:15px">{base_path}/{version}/meetings/{pathv1}/recordings?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">recordingDeleteOne(meetingId, recordingId, action = 'trash', callback)</td>
    <td style="padding:15px">Delete one meeting recording file</td>
    <td style="padding:15px">{base_path}/{version}/meetings/{pathv1}/recordings/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">recordingStatusUpdate(meetingId, body, callback)</td>
    <td style="padding:15px">Recover a meeting's recordings</td>
    <td style="padding:15px">{base_path}/{version}/meetings/{pathv1}/recordings/status?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">recordingStatusUpdateOne(meetingId, recordingId, body, callback)</td>
    <td style="padding:15px">Recover a single recording</td>
    <td style="padding:15px">{base_path}/{version}/meetings/{pathv1}/recordings/{pathv2}/status?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">recordingSettingUpdate(meetingId, callback)</td>
    <td style="padding:15px">Retrieve a meeting recording's settings</td>
    <td style="padding:15px">{base_path}/{version}/meetings/{pathv1}/recordings/settings?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">recordingSettingsUpdate(meetingId, body, callback)</td>
    <td style="padding:15px">Update a meeting recording's settings</td>
    <td style="padding:15px">{base_path}/{version}/meetings/{pathv1}/recordings/settings?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">dashboardMeetings(type = 'past', from, to, pageSize, nextPageToken, callback)</td>
    <td style="padding:15px">List meetings</td>
    <td style="padding:15px">{base_path}/{version}/metrics/meetings?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">dashboardMeetingDetail(meetingId, type = 'past', callback)</td>
    <td style="padding:15px">Retrieve meeting detail</td>
    <td style="padding:15px">{base_path}/{version}/metrics/meetings/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">dashboardMeetingParticipants(meetingId, type = 'past', pageSize, nextPageToken, callback)</td>
    <td style="padding:15px">Retrieve meeting participants</td>
    <td style="padding:15px">{base_path}/{version}/metrics/meetings/{pathv1}/participants?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">dashboardMeetingParticipantQOS(meetingId, participantId, type = 'past', callback)</td>
    <td style="padding:15px">Retrieve meeting participant QOS</td>
    <td style="padding:15px">{base_path}/{version}/metrics/meetings/{pathv1}/participants/{pathv2}/qos?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">dashboardMeetingParticipantsQOS(meetingId, type = 'past', pageSize, nextPageToken, callback)</td>
    <td style="padding:15px">List meeting participants QOS</td>
    <td style="padding:15px">{base_path}/{version}/metrics/meetings/{pathv1}/participants/qos?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">dashboardMeetingParticipantShare(meetingId, type = 'past', pageSize, nextPageToken, callback)</td>
    <td style="padding:15px">Retrieve sharing/recording details of meeting participant</td>
    <td style="padding:15px">{base_path}/{version}/metrics/meetings/{pathv1}/participants/sharing?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">dashboardWebinars(type = 'past', from, to, pageSize, nextPageToken, callback)</td>
    <td style="padding:15px">List webinars</td>
    <td style="padding:15px">{base_path}/{version}/metrics/webinars?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">dashboardWebinarDetail(webinarId, type = 'past', callback)</td>
    <td style="padding:15px">Retrieve webinar detail</td>
    <td style="padding:15px">{base_path}/{version}/metrics/webinars/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">dashboardWebinarParticipants(webinarId, type = 'past', pageSize, nextPageToken, callback)</td>
    <td style="padding:15px">Retrieve webinar participants</td>
    <td style="padding:15px">{base_path}/{version}/metrics/webinars/{pathv1}/participants?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">dashboardWebinarParticipantQOS(webinarId, participantId, type = 'past', callback)</td>
    <td style="padding:15px">Retrieve webinar participant QOS</td>
    <td style="padding:15px">{base_path}/{version}/metrics/webinars/{pathv1}/participants/{pathv2}/qos?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">dashboardWebinarParticipantsQOS(webinarId, type = 'past', pageSize, nextPageToken, callback)</td>
    <td style="padding:15px">List webinar participant QOS</td>
    <td style="padding:15px">{base_path}/{version}/metrics/webinars/{pathv1}/participants/qos?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">dashboardWebinarParticipantShare(webinarId, type = 'past', pageSize, nextPageToken, callback)</td>
    <td style="padding:15px">Retrieve sharing/recording details of webinar participant</td>
    <td style="padding:15px">{base_path}/{version}/metrics/webinars/{pathv1}/participants/sharing?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">dashboardZoomRooms(pageSize, pageNumber, callback)</td>
    <td style="padding:15px">List Zoom Rooms</td>
    <td style="padding:15px">{base_path}/{version}/metrics/zoomrooms?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">dashboardZoomRoom(zoomroomId, from, to, pageSize, pageNumber, callback)</td>
    <td style="padding:15px">Retrieve Zoom Room</td>
    <td style="padding:15px">{base_path}/{version}/metrics/zoomrooms/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">dashboardCRC(from, to, callback)</td>
    <td style="padding:15px">Retrieve CRC Port Usage</td>
    <td style="padding:15px">{base_path}/{version}/metrics/crc?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">dashboardIM(from, to, pageSize, nextPageToken, callback)</td>
    <td style="padding:15px">Retrieve IM</td>
    <td style="padding:15px">{base_path}/{version}/metrics/im?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">reportDaily(year, month, callback)</td>
    <td style="padding:15px">Retrieve daily report</td>
    <td style="padding:15px">{base_path}/{version}/report/daily?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">reportUsers(type = 'active', from, to, pageSize, pageNumber, callback)</td>
    <td style="padding:15px">Retrieve hosts report</td>
    <td style="padding:15px">{base_path}/{version}/report/users?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">reportMeetings(userId, from, to, pageSize, nextPageToken, callback)</td>
    <td style="padding:15px">Retrieve meetings report</td>
    <td style="padding:15px">{base_path}/{version}/report/users/{pathv1}/meetings?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">reportMeetingDetails(meetingId, callback)</td>
    <td style="padding:15px">Retrieve meeting details report</td>
    <td style="padding:15px">{base_path}/{version}/report/meetings/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">reportMeetingParticipants(meetingId, pageSize, nextPageToken, callback)</td>
    <td style="padding:15px">Retrieve meeting participants report</td>
    <td style="padding:15px">{base_path}/{version}/report/meetings/{pathv1}/participants?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">reportMeetingPolls(meetingId, callback)</td>
    <td style="padding:15px">Retrieve meeting polls report</td>
    <td style="padding:15px">{base_path}/{version}/report/meetings/{pathv1}/polls?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">reportWebinarDetails(webinarId, callback)</td>
    <td style="padding:15px">Retrieve webinar details report</td>
    <td style="padding:15px">{base_path}/{version}/report/webinars/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">reportWebinarParticipants(webinarId, pageSize, nextPageToken, callback)</td>
    <td style="padding:15px">Retrieve webinar participants report</td>
    <td style="padding:15px">{base_path}/{version}/report/webinars/{pathv1}/participants?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">reportWebinarPolls(webinarId, callback)</td>
    <td style="padding:15px">Retrieve webinar polls report</td>
    <td style="padding:15px">{base_path}/{version}/report/webinars/{pathv1}/polls?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">reportWebinarQA(webinarId, callback)</td>
    <td style="padding:15px">Retrieve webinar Q&A report</td>
    <td style="padding:15px">{base_path}/{version}/report/webinars/{pathv1}/qa?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">reportTelephone(type, from, to, pageSize, pageNumber, callback)</td>
    <td style="padding:15px">Retrieve telephone report</td>
    <td style="padding:15px">{base_path}/{version}/report/telephone?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">reportCloudRecording(from, to, callback)</td>
    <td style="padding:15px">Retrieve cloud recording usage report</td>
    <td style="padding:15px">{base_path}/{version}/report/cloud_recording?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTsp(callback)</td>
    <td style="padding:15px">Retrieve account's TSP information</td>
    <td style="padding:15px">{base_path}/{version}/tsp?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchTsp(body, callback)</td>
    <td style="padding:15px">Update account's TSP information</td>
    <td style="padding:15px">{base_path}/{version}/tsp?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">userTSPs(userId, callback)</td>
    <td style="padding:15px">List user's TSP accounts</td>
    <td style="padding:15px">{base_path}/{version}/users/{pathv1}/tsp?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">userTSPCreate(userId, body, callback)</td>
    <td style="padding:15px">Add a user's TSP account</td>
    <td style="padding:15px">{base_path}/{version}/users/{pathv1}/tsp?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">userTSP(userId, tspId, callback)</td>
    <td style="padding:15px">Retrieve a user's TSP account</td>
    <td style="padding:15px">{base_path}/{version}/users/{pathv1}/tsp/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">userTSPUpdate(userId, tspId, body, callback)</td>
    <td style="padding:15px">Update a TSP account</td>
    <td style="padding:15px">{base_path}/{version}/users/{pathv1}/tsp/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">userTSPDelete(userId, tspId, callback)</td>
    <td style="padding:15px">Delete a user's TSP account</td>
    <td style="padding:15px">{base_path}/{version}/users/{pathv1}/tsp/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getUsers(status = 'active', pageSize, pageNumber, callback)</td>
    <td style="padding:15px">List Users</td>
    <td style="padding:15px">{base_path}/{version}/users?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">userCreate(body, callback)</td>
    <td style="padding:15px">Create a user</td>
    <td style="padding:15px">{base_path}/{version}/users?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">user(userId, loginType, callback)</td>
    <td style="padding:15px">Retrieve a user</td>
    <td style="padding:15px">{base_path}/{version}/users/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">userUpdate(userId, body, callback)</td>
    <td style="padding:15px">Update a user</td>
    <td style="padding:15px">{base_path}/{version}/users/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">userDelete(userId, action = 'disassociate', transferEmail, transferMeeting, transferWebinar, transferRecording, callback)</td>
    <td style="padding:15px">Delete a user</td>
    <td style="padding:15px">{base_path}/{version}/users/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">userAssistants(userId, callback)</td>
    <td style="padding:15px">List a user's assistants</td>
    <td style="padding:15px">{base_path}/{version}/users/{pathv1}/assistants?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">userAssistantCreate(userId, body, callback)</td>
    <td style="padding:15px">Add assistants</td>
    <td style="padding:15px">{base_path}/{version}/users/{pathv1}/assistants?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">userAssistantsDelete(userId, callback)</td>
    <td style="padding:15px">Delete a user's assistants</td>
    <td style="padding:15px">{base_path}/{version}/users/{pathv1}/assistants?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">userAssistantDelete(userId, assistantId, callback)</td>
    <td style="padding:15px">Delete a user's assistant</td>
    <td style="padding:15px">{base_path}/{version}/users/{pathv1}/assistants/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">userSchedulers(userId, callback)</td>
    <td style="padding:15px">List a user's schedulers</td>
    <td style="padding:15px">{base_path}/{version}/users/{pathv1}/schedulers?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">userSchedulersDelete(userId, callback)</td>
    <td style="padding:15px">Delete a user's schedulers</td>
    <td style="padding:15px">{base_path}/{version}/users/{pathv1}/schedulers?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">userSchedulerDelete(userId, schedulerId, callback)</td>
    <td style="padding:15px">Delete a user's scheduler</td>
    <td style="padding:15px">{base_path}/{version}/users/{pathv1}/schedulers/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">userPicture(userId, picFile, callback)</td>
    <td style="padding:15px">Upload a user's picture</td>
    <td style="padding:15px">{base_path}/{version}/users/{pathv1}/picture?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">userSettings(userId, loginType, callback)</td>
    <td style="padding:15px">Retrieve a user's settings</td>
    <td style="padding:15px">{base_path}/{version}/users/{pathv1}/settings?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">userSettingsUpdate(userId, body, callback)</td>
    <td style="padding:15px">Update a user's settings</td>
    <td style="padding:15px">{base_path}/{version}/users/{pathv1}/settings?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">userStatus(userId, body, callback)</td>
    <td style="padding:15px">Update a user's status</td>
    <td style="padding:15px">{base_path}/{version}/users/{pathv1}/status?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">userPassword(userId, body, callback)</td>
    <td style="padding:15px">Update a user's password</td>
    <td style="padding:15px">{base_path}/{version}/users/{pathv1}/password?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">userPermission(userId, callback)</td>
    <td style="padding:15px">Retrieve a user's permissions</td>
    <td style="padding:15px">{base_path}/{version}/users/{pathv1}/permissions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">userToken(userId, type = 'token', callback)</td>
    <td style="padding:15px">Retrieve a user's token</td>
    <td style="padding:15px">{base_path}/{version}/users/{pathv1}/token?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">userSSOTokenDelete(userId, callback)</td>
    <td style="padding:15px">Revoke a user's SSO token</td>
    <td style="padding:15px">{base_path}/{version}/users/{pathv1}/token?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">userEmailUpdate(userId, body, callback)</td>
    <td style="padding:15px">Update a user's email</td>
    <td style="padding:15px">{base_path}/{version}/users/{pathv1}/email?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">userZPK(zpk, callback)</td>
    <td style="padding:15px">Verify a user's zpk (Deprecated</td>
    <td style="padding:15px">{base_path}/{version}/users/zpk?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">userEmail(email, callback)</td>
    <td style="padding:15px">Check a user's email</td>
    <td style="padding:15px">{base_path}/{version}/users/email?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">userVanityName(vanityName, callback)</td>
    <td style="padding:15px">Check a user's personal meeting room name</td>
    <td style="padding:15px">{base_path}/{version}/users/vanity_name?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">userPACs(userId, callback)</td>
    <td style="padding:15px">List user's PAC accounts</td>
    <td style="padding:15px">{base_path}/{version}/users/{pathv1}/pac?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">webinars(userId, pageSize, pageNumber, callback)</td>
    <td style="padding:15px">List webinars</td>
    <td style="padding:15px">{base_path}/{version}/users/{pathv1}/webinars?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">webinarCreate(userId, body, callback)</td>
    <td style="padding:15px">Create a webinar</td>
    <td style="padding:15px">{base_path}/{version}/users/{pathv1}/webinars?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">webinar(webinarId, callback)</td>
    <td style="padding:15px">Retrieve a webinar</td>
    <td style="padding:15px">{base_path}/{version}/webinars/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">webinarUpdate(webinarId, body, callback)</td>
    <td style="padding:15px">Update a webinar</td>
    <td style="padding:15px">{base_path}/{version}/webinars/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">webinarDelete(webinarId, occurrenceId, callback)</td>
    <td style="padding:15px">Delete a webinar</td>
    <td style="padding:15px">{base_path}/{version}/webinars/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">webinarStatus(webinarId, body, callback)</td>
    <td style="padding:15px">Update a webinar's status</td>
    <td style="padding:15px">{base_path}/{version}/webinars/{pathv1}/status?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">webinarPanelists(webinarId, callback)</td>
    <td style="padding:15px">List a webinar's panelists</td>
    <td style="padding:15px">{base_path}/{version}/webinars/{pathv1}/panelists?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">webinarPanelistCreate(webinarId, body, callback)</td>
    <td style="padding:15px">Add a webinar panelist</td>
    <td style="padding:15px">{base_path}/{version}/webinars/{pathv1}/panelists?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">webinarPanelistsDelete(webinarId, callback)</td>
    <td style="padding:15px">Remove a webinar's panelists</td>
    <td style="padding:15px">{base_path}/{version}/webinars/{pathv1}/panelists?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">webinarPanelistDelete(webinarId, panelistId, callback)</td>
    <td style="padding:15px">Remove a webinar panelist</td>
    <td style="padding:15px">{base_path}/{version}/webinars/{pathv1}/panelists/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">webinarRegistrants(webinarId, occurrenceId, status = 'pending', pageSize, pageNumber, callback)</td>
    <td style="padding:15px">List a webinar's registrants</td>
    <td style="padding:15px">{base_path}/{version}/webinars/{pathv1}/registrants?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">webinarRegistrantCreate(webinarId, occurrenceIds, body, callback)</td>
    <td style="padding:15px">Add a webinar registrant</td>
    <td style="padding:15px">{base_path}/{version}/webinars/{pathv1}/registrants?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">webinarRegistrantStatus(webinarId, occurrenceId, body, callback)</td>
    <td style="padding:15px">Update a webinar registrant's status</td>
    <td style="padding:15px">{base_path}/{version}/webinars/{pathv1}/registrants/status?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">pastWebinars(webinarId, callback)</td>
    <td style="padding:15px">List of ended webinar instances</td>
    <td style="padding:15px">{base_path}/{version}/past_webinars/{pathv1}/instances?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">webinarPolls(webinarId, callback)</td>
    <td style="padding:15px">List a webinar's polls</td>
    <td style="padding:15px">{base_path}/{version}/webinars/{pathv1}/polls?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">webinarPollCreate(webinarId, body, callback)</td>
    <td style="padding:15px">Create a webinar's poll</td>
    <td style="padding:15px">{base_path}/{version}/webinars/{pathv1}/polls?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">webinarPollGet(webinarId, pollId, callback)</td>
    <td style="padding:15px">Retrieve a webinar's poll</td>
    <td style="padding:15px">{base_path}/{version}/webinars/{pathv1}/polls/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">webinarPollUpdate(webinarId, pollId, body, callback)</td>
    <td style="padding:15px">Update a webinar's poll</td>
    <td style="padding:15px">{base_path}/{version}/webinars/{pathv1}/polls/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">webinarPollDelete(webinarId, pollId, callback)</td>
    <td style="padding:15px">Delete a webinar's Poll</td>
    <td style="padding:15px">{base_path}/{version}/webinars/{pathv1}/polls/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">webhookSwitch(body, callback)</td>
    <td style="padding:15px">Switch webhook version</td>
    <td style="padding:15px">{base_path}/{version}/webhooks/options?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getWebhooks(callback)</td>
    <td style="padding:15px">List webhooks</td>
    <td style="padding:15px">{base_path}/{version}/webhooks?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">webhookCreate(body, callback)</td>
    <td style="padding:15px">Create a webhook</td>
    <td style="padding:15px">{base_path}/{version}/webhooks?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">webhook(webhookId, callback)</td>
    <td style="padding:15px">Retrieve a webhook</td>
    <td style="padding:15px">{base_path}/{version}/webhooks/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">webhookUpdate(webhookId, body, callback)</td>
    <td style="padding:15px">Update a webhook</td>
    <td style="padding:15px">{base_path}/{version}/webhooks/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">webhookDelete(webhookId, callback)</td>
    <td style="padding:15px">Delete a webhook</td>
    <td style="padding:15px">{base_path}/{version}/webhooks/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
