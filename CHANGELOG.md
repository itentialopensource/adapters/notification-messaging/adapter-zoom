
## 0.4.4 [10-15-2024]

* Changes made at 2024.10.14_20:49PM

See merge request itentialopensource/adapters/adapter-zoom!17

---

## 0.4.3 [09-20-2024]

* add workshop and fix vulnerabilities

See merge request itentialopensource/adapters/adapter-zoom!15

---

## 0.4.2 [08-14-2024]

* Changes made at 2024.08.14_19:06PM

See merge request itentialopensource/adapters/adapter-zoom!14

---

## 0.4.1 [08-07-2024]

* Changes made at 2024.08.06_20:20PM

See merge request itentialopensource/adapters/adapter-zoom!13

---

## 0.4.0 [07-19-2024]

* Minor/2024 auto migration

See merge request itentialopensource/adapters/notification-messaging/adapter-zoom!12

---

## 0.3.3 [03-29-2024]

* Changes made at 2024.03.29_10:20AM

See merge request itentialopensource/adapters/notification-messaging/adapter-zoom!11

---

## 0.3.2 [03-11-2024]

* Changes made at 2024.03.11_11:12AM

See merge request itentialopensource/adapters/notification-messaging/adapter-zoom!10

---

## 0.3.1 [02-28-2024]

* Changes made at 2024.02.28_12:31PM

See merge request itentialopensource/adapters/notification-messaging/adapter-zoom!9

---

## 0.3.0 [01-01-2024]

* Adapter Engine has been updated and the changes are being migrated to the adapter

See merge request itentialopensource/adapters/notification-messaging/adapter-zoom!8

---

## 0.2.0 [05-27-2022]

* Migration to the latest Adapter Foundation

See merge request itentialopensource/adapters/notification-messaging/adapter-zoom!2

---

## 0.1.3 [03-16-2021]

- Migration to bring up to the latest foundation
  - Change to .eslintignore (adapter_modification directory)
  - Change to README.md (new properties, new scripts, new processes)
  - Changes to adapterBase.js (new methods)
  - Changes to package.json (new scripts, dependencies)
  - Changes to propertiesSchema.json (new properties and changes to existing)
  - Changes to the Unit test
  - Adding several test files, utils files and .generic entity
  - Fix order of scripts and dependencies in package.json
  - Fix order of properties in propertiesSchema.json
  - Update sampleProperties, unit and integration tests to have all new properties.
  - Add all new calls to adapter.js and pronghorn.json
  - Add suspend piece to older methods

See merge request itentialopensource/adapters/notification-messaging/adapter-zoom!1

---

## 0.1.2 [09-21-2020] & 0.1.1 [09-21-2020]

- Initial Commit

See commit 2752f74

---
